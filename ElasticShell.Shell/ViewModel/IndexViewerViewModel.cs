﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Service.Helper;
using ElasticShell.Service.Model;
using Type = ElasticShell.Service.Model.Type;

namespace ElasticShell.Shell.ViewModel
{
    public class IndexViewerViewModel : BaseWindowViewModel
    {
        public delegate void TypeSelectedHandler(DataBase database, Index index, Type type);

        public event EventHandler ExcuteEvent; 

        private DataBase _dataBase;
        private ICommand _deleteCommand;
        private string _indexPath;
        private Index _selectedIndex;
        private Type _selectedType;
        private ICommand _excuteCommand;
        private ICommand _copyCommand;
        private string _searchText;

        public IndexViewerViewModel()
        {
            DataBase = new DataBase();
        }

        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new DelegateCommand(Delete));
        public ICommand ExcuteCommand => _excuteCommand ?? (_excuteCommand = new DelegateCommand(Excute));
        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new DelegateCommand(Copy));

        private void Copy()
        {
            if(IndexPath!=null)
            {
                Clipboard.SetText(IndexPath);
            }
        }

        private void Excute()
        {
            OnExcute();
        }

        public DataBase DataBase
        {
            get { return _dataBase; }
            set
            {
                _dataBase = value;
                value.TypeSelected += Value_TypeSelected;
                RaisePropertyChanged(() => DataBase);
            }
        }

        public string IndexPath
        {
            get { return _indexPath; }
            set
            {
                _indexPath = value;
                RaisePropertyChanged(() => IndexPath);
            }
        }
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                foreach (var index in DataBase.Indices)
                {
                    index.IsHighlighted = !string.IsNullOrEmpty(value) && index.Name.Contains(_searchText) ;
                    foreach (var type in index.Types)
                    {
                        type.IsHighlighted = !string.IsNullOrEmpty(value) && type.Name.Contains(_searchText) ;
                    }
                }
                RaisePropertyChanged(() => _searchText);
            }
        }
        public event TypeSelectedHandler TypeSelected;

        private void Delete()
        {
            if (_selectedIndex != null)
            {
                if (_selectedType == null)
                {
                    var t = MessageBox.Show("Are you sure you want to delete the index " + IndexPath + "?", "ElasticShell",
                         MessageBoxButton.YesNo);
                    if (t == MessageBoxResult.Yes)
                    {

                        ElesticHelper.GetInstance().DeteteByIndex(_selectedIndex.Name);
                        MessageHelper.Reload();
                    }
                }
                else
                {
                    var t = MessageBox.Show("Are you sure you want to delete all documents under " + IndexPath + "?",
                        "ElasticShell", MessageBoxButton.YesNo);
                    if (t == MessageBoxResult.Yes)
                    {
                        IsWorking = true;
                        var task = Task.Factory.StartNew(() =>
                        {
                            ElesticHelper.GetInstance().DeteteByType(_selectedIndex.Name, _selectedType.Name);
                            Thread.Sleep(1000);
                        });
                        task.GetAwaiter().OnCompleted(() =>
                        {
                            IsWorking = false;
                            MessageHelper.Reload();
                        });
                    }
                }
            }
        }

        private void Value_TypeSelected(DataBase database, Index index, Type type)
        {
            if (type == null)
            {
                IndexPath = $"{index.Name}";
                _selectedType = null;
                _selectedIndex = index;
            }
            else
            {
                IndexPath = $"{index.Name}/{type.Name}";
                _selectedType = type;
                _selectedIndex = index;
            }
            OnTypeSelected(database, index, type);
        }


        protected virtual void OnTypeSelected(DataBase database, Index index, Type type)
        {
            TypeSelected?.Invoke(database, index, type);
        }

        protected virtual void OnExcute()
        {
            ExcuteEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}