﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ElasticShell.Service.Model;
using Type = ElasticShell.Service.Model.Type;

namespace ElasticShell.Shell.ViewModel
{
    public class QueryBuilderWindowViewModel:BaseWindowViewModel
    {
        private DataBase _dataBase;

        public delegate void ChangeQueryHandler(string query);

        public event ChangeQueryHandler ChangeQuery;
        public ObservableCollection<TabItem> Tabs { get; set; }
        protected virtual void OnChangeQuery(string query)
        {
            ChangeQuery?.Invoke(query);
        }

        public QueryBuilderViewModel QueryBuilderViewModel { get; set; }

        public DataBase DataBase
        {
            get { return _dataBase; }
            set
            {
                _dataBase = value;
                QueryBuilderViewModel.DataBase = _dataBase;
            }
        }


        public void IndexTypeChanged(Index selectedIndex, Type selectedType)
        {
            QueryBuilderViewModel.SetIndex(selectedIndex,selectedType);
        }

        public QueryBuilderWindowViewModel()
        {
            QueryBuilderViewModel= new QueryBuilderViewModel();

        }

    }

}
