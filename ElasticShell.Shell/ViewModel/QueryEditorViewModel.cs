﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Components;
using ElasticShell.Service.Helper;
using ElasticShell.Service.Model;
using ElasticShell.Shell.Window;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Type = ElasticShell.Service.Model.Type;

namespace ElasticShell.Shell.ViewModel
{
    public class QueryEditorViewModel : BaseWindowViewModel
    {
        public delegate void SetResultHandler(object result);

        private readonly QueryBuilderWindowViewModel _queryBuilder;
        private ICommand _addCommand;

        private ICommand _builderCommand;
        private ICommand _deleteCommand;
        private ICommand _deleteTabCommand;
        private ICommand _indentCommand;
        private bool _indentString;

        private bool _isRunable;
        private ICommand _loadCommand;
        private string _loadTabsData;
        private string _queryText;
        private ICommand _runCommand;
        private ICommand _saveCommand;
        private string _saveTabData;
        private string _selectedDocumentType = "Source";

        private Index _selectedIndex;
        private Type _selectedType;
        private int _size;
        private WindowManager.WindowObject _window;
        private Query _query;
        private List<string> _sortItems;
        private string _selectedSortItem;
        private DataBase _dataBase;

        public QueryEditorViewModel()
        {
            From = 0;
            Size = 20;
            DocumentTypes = new List<string>();
            DocumentTypes.Add("Hits");
            DocumentTypes.Add("Source");
            QueryText = GetQueryText();
            _queryBuilder = new QueryBuilderWindowViewModel();
            _window = WindowManager.SetWindow(new QueryBuilderWindow(), _queryBuilder);
            _queryBuilder.ChangeQuery += QueryEditorViewModel_ChangeQuery;
            TextReader tr =
                new StreamReader(Assembly.GetEntryAssembly()
                    .GetManifestResourceStream("ElasticShell.Shell.Resources.elasticQuery.json"));
            var str = tr.ReadToEnd();
            Query = new Query {QueryItems = JsonConvert.DeserializeObject<List<QueryItem>>(str)};

        }

        public int From { get; set; }

        public string SelectedSortItem
        {
            get
            {
                return _selectedSortItem;
            }
            set
            {
                _selectedSortItem = value; 
                RaisePropertyChanged(() => SortItems);
            }
        }

        public List<string> SortItems
        {
            get { return _sortItems; }
            set
            {
                _sortItems = value;
                RaisePropertyChanged(() => SortItems);
            }
        }
        public bool IndentString
        {
            get { return _indentString; }
            set
            {
                _indentString = value;
                RaisePropertyChanged(() => IndentString);
            }
        }

        public string SelectedTabCode
        {
            get { return _queryText; }
            set
            {
                _queryText = value;
                RaisePropertyChanged(() => SelectedTabCode);
            }
        }

        public string QueryText
        {
            get { return _queryText; }
            set
            {
                _queryText = value;
                RaisePropertyChanged(() => QueryText);
            }
        }
        public Query Query
        {
            get { return _query; }
            set
            {
                _query = value;
                RaisePropertyChanged(() => Query);
            }
        }
        public string LoadTabsData
        {
            get { return _loadTabsData; }
            set
            {
                _loadTabsData = value;
                RaisePropertyChanged(() => LoadTabsData);
            }
        }

        public string SaveTabData
        {
            get { return _saveTabData; }
            set
            {
                _saveTabData = value;
                RaisePropertyChanged(() => SaveTabData);
            }
        }

        public int Size
        {
            get { return _size; }
            set
            {
                _size = value;
                if (value > 500)
                {
                    _size = 500;
                }
                RaisePropertyChanged(() => Size);
            }
        }

        public ICommand RunCommand => _runCommand ?? (_runCommand = new DelegateCommand(Run));
        public ICommand BuilderCommand => _builderCommand ?? (_builderCommand = new DelegateCommand(Builder));
        public ICommand IndentCommand => _indentCommand ?? (_indentCommand = new DelegateCommand(Indent));
        public ICommand AddCommand => _addCommand ?? (_addCommand = new DelegateCommand(Add));
        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new DelegateCommand(Delete));
        public ICommand LoadCommand => _loadCommand ?? (_loadCommand = new DelegateCommand(Load));
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new DelegateCommand(Save));
        public ICommand DeleteTabCommand => _deleteTabCommand ?? (_deleteTabCommand = new DelegateCommand(DeleteTab));

        public string SelectedDocumentType
        {
            get { return _selectedDocumentType; }
            set
            {
                _selectedDocumentType = value;
                RaisePropertyChanged(() => SelectedDocumentType);
            }
        }

        public List<string> DocumentTypes { get; set; }

        public bool IsRunable
        {
            get { return _isRunable; }
            set
            {
                _isRunable = value;
                RaisePropertyChanged(() => IsRunable);
            }
        }

        public DataBase DataBase
        {
            get { return _dataBase; }
            set
            {
                _dataBase = value;
                _queryBuilder.DataBase = _dataBase;
            }
        }

        private void DeleteTab()
        {
            if (
                MessageBox.Show("Do you want to revome it from database also?", "ElasticShell", MessageBoxButton.YesNo) ==
                MessageBoxResult.Yes)
            {
                ElesticHelper.GetInstance().Delete("elasticshell", "tabdata", SelectedTabCode);
            }
        }

        private void Load()
        {
            dynamic data = (List<object>) ElesticHelper.GetInstance().Search(0, "elasticshell", "tabdata", 0, 20);
            var newList = new List<object>();
            foreach (var obj in data)
            {
                if (obj.Type == "query")
                {
                    newList.Add(obj);
                }
            }
            LoadTabsData = JsonConvert.SerializeObject(newList);
            //LoadTabsData = JsonConvert.SerializeObject(data);
        }


        private void Save()
        {
            dynamic obj = JsonConvert.DeserializeObject(SaveTabData);
            var data = (List<object>) ElesticHelper.GetInstance().Search(0, "elasticshell", "tabdata", 0, 20);
            if (data.Count == 0)
                data = new List<object>();
            obj.Type = "query";
            data.Add(obj);
            obj.Result = obj;
            ElesticHelper.GetInstance().Add("elasticshell", "tabdata", obj.Code.ToString(), obj);
        }

        private void Delete()
        {
            try
            {
                var id = Interaction.InputBox("Please type the id of the document you want to delete.", "Elastic Shell",
                    "", -1, -1);
                if (id != string.Empty)
                {
                    if (ElesticHelper.GetInstance().Delete(_selectedIndex.Name, _selectedType.Name, id))
                    {
                        _selectedType.DocumentCount--;
                        MessageBox.Show("Document Deleted.", "Elastic Shell");
                    }
                    else
                    {
                        MessageBox.Show("Deleting the documant has failed", "Elastic Shell");
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Deleting the documant has failed", "Elastic Shell");
            }
        }

        private void Add()
        {
            try
            {
                dynamic obj = null;
                var did = "";

                try
                {
                    obj = JsonConvert.DeserializeObject(QueryText);
                    foreach (var prop in obj)
                    {
                        var name = prop.Path.ToString().ToLower();
                        if (name.Contains("id") || name.Contains("key") || name.Contains("code"))
                        {
                            did = prop.Value;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Invallid Json.", "Elastic Shell");
                    return;
                }

                var id = Interaction.InputBox("Please type the id, if the id exist the document will be overwriten.",
                    "Elastic Shell", did, -1, -1);
                if (id != string.Empty)
                {
                    if (ElesticHelper.GetInstance().Add(_selectedIndex.Name, _selectedType.Name, id, obj))
                    {
                        _selectedType.DocumentCount++;
                    }

                    MessageBox.Show("Document Saved.", "Elastic Shell");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Saving the documant has failed", "Elastic Shell");
            }
        }

        private void Indent()
        {
            IndentString = true;
            IndentString = false;
        }

        private string GetQueryText()
        {
            dynamic q = new ExpandoObject();
            q.match_all = new ExpandoObject();
            return JsonConvert.SerializeObject(q, Formatting.Indented);
        }

        private void Builder()
        {
            _window.Show();
            _window.Window.Topmost = true;
            _window.Window.Topmost = false;
        }

        private void QueryEditorViewModel_ChangeQuery(string query)
        {
            QueryText = query;
        }

        public event SetResultHandler SetResult;

        public void Run()
        {
            if (_selectedType == null)
                return;
            if (_selectedDocumentType == "Hits")
                OnSetResult(ElesticHelper.GetInstance()
                    .Search(1, _selectedIndex.Name, _selectedType.Name, From, Size, QueryText,SelectedSortItem));
            else
                OnSetResult(ElesticHelper.GetInstance()
                    .Search(0, _selectedIndex.Name, _selectedType.Name, From, Size, QueryText, SelectedSortItem));
        }

        public void SetIndex(Index selectedIndex, Type selectedType)
        {
            _selectedIndex = selectedIndex;
            _selectedType = selectedType;
            if (_selectedType == null)
            {
                IsRunable = false;
                //_window.Window.Hide();
            }
            else
            {
                IsRunable = true;
                _queryBuilder.IndexTypeChanged(_selectedIndex, _selectedType);
                SortItems = ElesticHelper.GetInstance()
                    .MappingPropertyNames( _selectedIndex.Name, _selectedType.Name);
                SortItems.Insert(0,"");
            }
            _queryBuilder.IndexTypeChanged(selectedIndex, selectedType);

        }

        protected virtual void OnSetResult(object result)
        {
            SetResult?.Invoke(result);
        }
    }
}