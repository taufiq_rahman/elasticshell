﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Components;
using ElasticShell.Service.Helper;
using ElasticShell.Service.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Type = ElasticShell.Service.Model.Type;

namespace ElasticShell.Shell.ViewModel
{
    public class QueryBuilderViewModel : BaseWindowViewModel
    {
        public delegate void SetResultHandler(object result);

        private ICommand _addCommand;
        private List<string> _apiList;
        private DataBase _dataBase;
        private ICommand _deleteCommand;
        private ICommand _deleteTabCommand;
        private ICommand _indentCommand;
        private bool _indentString;
        private List<Index> _indiceList;
        private bool _isEditable;

        private bool _isRunable;
        private ICommand _loadCommand;
        private string _loadTabsData;

        private Query _query;
        private string _queryText;
        private ICommand _runCommand;
        private ICommand _saveCommand;
        private string _saveTabData;
        private string _selectedApiItem;
        private string _selectedDocumentType = "Source";


        private Index _selectedIndexItem;
        private Type _selectedTypeItem;
        private int _size;
        private List<Type> _typeList;
        private string _commandText;
        private ICommand _buildCommand;
        private string _selectedCommandItem;
        private bool _typeCombo;

        public QueryBuilderViewModel()
        {
            //_queryBuilder.ChangeQuery += QueryEditorViewModel_ChangeQuery;
            TextReader tr =
                new StreamReader(Assembly.GetEntryAssembly()
                    .GetManifestResourceStream("ElasticShell.Shell.Resources.elasticQuery.json"));
            var str = tr.ReadToEnd();
            Query = new Query { QueryItems = JsonConvert.DeserializeObject<List<QueryItem>>(str) };
            DocumentViewer = new DocumentViewerViewModel();
            ApiList = new List<string> { "_search", "_mapping" };
            CommandList = new List<string>() { "GET", "DELETE" };
            SelectedApiItem = "_search";
            SelectedCommandItem = "GET";
        }

        public DocumentViewerViewModel DocumentViewer { get; set; }

        public bool TypeCombo
        {
            get { return _typeCombo; }
            set
            {
                _typeCombo = value;
                RaisePropertyChanged(()=> TypeCombo);

            }
        }

        public string SelectedCommandItem
        {
            get { return _selectedCommandItem; }
            set
            {
                _selectedCommandItem = value;
                if (value == "GET")
                {
                    TypeCombo = true;
                }
                else
                {
                    TypeCombo = false;
                }
                RaisePropertyChanged(()=>SelectedCommandItem);
            }
        }

        public int From { get; set; }

        public string CommandText
        {
            get { return _commandText; }
            set
            {
                _commandText = value;
                RaisePropertyChanged(() => CommandText);
            }
        }

        public bool IndentString
        {
            get { return _indentString; }
            set
            {
                _indentString = value;
                RaisePropertyChanged(() => IndentString);
            }
        }

        public string SelectedTabCode
        {
            get { return _queryText; }
            set
            {
                _queryText = value;
                RaisePropertyChanged(() => SelectedTabCode);
            }
        }

        public string QueryText
        {
            get { return _queryText; }
            set
            {
                _queryText = value;
                RaisePropertyChanged(() => QueryText);
            }
        }

        public Query Query
        {
            get { return _query; }
            set
            {
                _query = value;
                RaisePropertyChanged(() => QueryText);
            }
        }

        public string LoadTabsData
        {
            get { return _loadTabsData; }
            set
            {
                _loadTabsData = value;
                RaisePropertyChanged(() => LoadTabsData);
            }
        }

        public string SaveTabData
        {
            get { return _saveTabData; }
            set
            {
                _saveTabData = value;
                RaisePropertyChanged(() => SaveTabData);
            }
        }

        public int Size
        {
            get { return _size; }
            set
            {
                _size = value;
                if (value > 500)
                {
                    _size = 500;
                }
                RaisePropertyChanged(() => Size);
            }
        }

        public List<string> ApiList
        {
            get { return _apiList; }
            set
            {
                _apiList = value;
                RaisePropertyChanged(() => ApiList);
            }
        }

        public string SelectedApiItem
        {
            get { return _selectedApiItem; }
            set
            {
                _selectedApiItem = value;
                IsEditable = value == "_search";

                RaisePropertyChanged(() => SelectedApiItem);
            }
        }

        public ICommand RunCommand => _runCommand ?? (_runCommand = new DelegateCommand(Run));
        public ICommand BuildCommand => _buildCommand ?? (_buildCommand = new DelegateCommand(Build));



        public ICommand IndentCommand => _indentCommand ?? (_indentCommand = new DelegateCommand(Indent));
        public ICommand LoadCommand => _loadCommand ?? (_loadCommand = new DelegateCommand(Load));
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new DelegateCommand(Save));
        public ICommand DeleteTabCommand => _deleteTabCommand ?? (_deleteTabCommand = new DelegateCommand(DeleteTab));

        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                RaisePropertyChanged(() => IsEditable);
            }
        }

        public string SelectedDocumentType
        {
            get { return _selectedDocumentType; }
            set
            {
                _selectedDocumentType = value;
                RaisePropertyChanged(() => SelectedDocumentType);
            }
        }

        public List<string> DocumentTypes { get; set; }
        public List<string> CommandList { get; set; }

        public bool IsRunable
        {
            get { return _isRunable; }
            set
            {
                _isRunable = value;
                RaisePropertyChanged(() => IsRunable);
            }
        }


        public List<Index> IndiceList
        {
            get { return _indiceList; }
            set
            {
                _indiceList = value;
                RaisePropertyChanged(() => IndiceList);
            }
        }

        public List<Type> TypeList
        {
            get { return _typeList; }
            set
            {
                _typeList = value;
                RaisePropertyChanged(() => TypeList);
            }
        }

        public Index SelectedIndexItem
        {
            get { return _selectedIndexItem; }
            set
            {

                _selectedIndexItem = value;
                if (value == null)
                {
                    SelectedTypeItem = null;
                }
                else
                {
                    TypeList = IndiceList.Find(q => q == SelectedIndexItem).Types.ToList();
                }
                UpdateCommand();
                RaisePropertyChanged(() => SelectedIndexItem);
            }
        }

        private void UpdateCommand()
        {

            //CommandText = $"";
        }
        private void Build()
        {
            if (SelectedCommandItem == "GET")
            {
                if (SelectedIndexItem == null)
                {
                    CommandText = $"{SelectedCommandItem} */{SelectedApiItem}";
                }
                else if (SelectedTypeItem == null)
                {
                    CommandText = $"{SelectedCommandItem} {SelectedIndexItem.Name}/{SelectedApiItem}";
                }
                else
                {
                    CommandText =
                        $"{SelectedCommandItem} {SelectedIndexItem.Name}/{SelectedTypeItem.Name}/{SelectedApiItem}";
                }
            }
            else
            {
                if (SelectedIndexItem != null)
                {
                    CommandText = $"{SelectedCommandItem} {SelectedIndexItem.Name}";
                }
            }
        }
        public Type SelectedTypeItem
        {
            get { return _selectedTypeItem; }
            set
            {
                _selectedTypeItem = value;
                RaisePropertyChanged(() => SelectedTypeItem);
            }
        }

        public DataBase DataBase
        {
            get { return _dataBase; }
            set
            {
                _dataBase = value;
                IndiceList = _dataBase.Indices.ToList();
            }
        }

        private void DeleteTab()
        {
            if (
                MessageBox.Show("Do you want to revome it from database also?", "ElasticShell", MessageBoxButton.YesNo) ==
                MessageBoxResult.Yes)
            {
                ElesticHelper.GetInstance().Delete("elasticshell", "tabdata", SelectedTabCode);
            }
        }

        private void Load()
        {
            dynamic data = (List<object>)ElesticHelper.GetInstance().Search(0, "elasticshell", "tabdata", 0, 20);
            var newList = new List<object>();
            foreach (var obj in data)
            {
                if (obj.Type == "dsl")
                {
                    newList.Add(obj);
                }
            }
            LoadTabsData = JsonConvert.SerializeObject(newList);

        }


        private void Save()
        {
            dynamic obj = JsonConvert.DeserializeObject(SaveTabData);
            var data = (List<object>)ElesticHelper.GetInstance().Search(0, "elasticshell", "tabdata", 0, 20);
            if (data.Count == 0)
                data = new List<object>();
            obj.Type = "dsl";
            data.Add(obj);
            ElesticHelper.GetInstance().Add("elasticshell", "tabdata", obj.Code.ToString(), obj);
        }

        private void Indent()
        {
            IndentString = true;
            IndentString = false;
        }


        public event SetResultHandler SetResult;

        public void Run()
        {
            dynamic result = ElesticHelper.GetInstance().Raw(CommandText, QueryText);
            if (result != null && result.hits != null && ((JArray)result.hits.hits).Count > 200)
            {
                var list = ((JArray)result.hits.hits).Skip(200).ToList();
                foreach (var jToken in list)
                {
                    ((JArray)result.hits.hits).Remove(jToken);
                }
            }
            DocumentViewer.DocumentString = result?.ToString() ?? "";
        }

        public void SetIndex(Index selectedIndex, Type selectedType)
        {
            SelectedIndexItem = selectedIndex;
            SelectedTypeItem = selectedType;

        }

        protected virtual void OnSetResult(object result)
        {
            //DocumentViewer.DocumentString = JsonConvert.SerializeObject(result);
            SetResult?.Invoke(result);
        }
    }
}