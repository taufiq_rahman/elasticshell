﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Service.Helper;
using ElasticShell.Service.Model;
using ElasticShell.Shell.Window;

namespace ElasticShell.Shell.ViewModel
{
    public class MainWindowViewModel : BaseWindowViewModel
    {
        private ICommand _aboutCommand;
        private ICommand _connectCommand;
        private ICommand _refreshCommand;

        private Visibility _isBusy = Visibility.Collapsed;
        private bool _isConnected;


        private Index _selectedIndex;
        private Type _selectedType;

        private Settings _settings;

        public MainWindowViewModel()
        {
            IndexViewer = new IndexViewerViewModel();
            IndexViewer.TypeSelected += IndexViewer_TypeSelected;
            IndexViewer.ExcuteEvent += IndexViewer_ExcuteEvent;
            QueryEditor = new QueryEditorViewModel();
            QueryEditor.SetResult += QueryEditor_SetResult;
            DocumantViewer = new DocumentViewerViewModel();
            MessageHelper.Busy += MessageHelper_Busy;
            MessageHelper.ReloadIndex += MessageHelper_ReloadIndex;
        }

        private void IndexViewer_ExcuteEvent(object sender, System.EventArgs e)
        {
            QueryEditor.Run();
        }

        public ICommand ConnectCommand => _connectCommand ?? (_connectCommand = new DelegateCommand(Connect));
        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new DelegateCommand(Refresh));
        public ICommand AboutCommand => _aboutCommand ?? (_aboutCommand = new DelegateCommand(About));

        public IndexViewerViewModel IndexViewer { get; set; }
        public QueryEditorViewModel QueryEditor { get; set; }

        public DocumentViewerViewModel DocumantViewer { get; set; }

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                RaisePropertyChanged(() => IsConnected);
            }
        }

        public Visibility BusyScreenVisibility
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => BusyScreenVisibility);
            }
        }


        public string IndexPath { get; set; }

        private void QueryEditor_SetResult(object result)
        {
            DocumantViewer.DisplayResult(result);
        }

        private void About()
        {
            var window = new AboutWindow();
            window.ShowDialog();
        }

        private void IndexViewer_TypeSelected(DataBase database, Index index, Type type)
        {
            if (type == null)
            {
                IndexPath = $"{index.Name}";
                _selectedType = null;
                _selectedIndex = index;
            }
            else
            {
                IndexPath = $"{index.Name}/{type.Name}";
                _selectedType = type;
                _selectedIndex = index;
            }
            QueryEditor.SetIndex(_selectedIndex, _selectedType);
        }

        private void MessageHelper_ReloadIndex()
        {
            LoadUI();
        }


        private void Refresh()
        {
            LoadUI();
        }

        private void MessageHelper_Busy(bool busy)
        {
            BusyScreenVisibility = busy ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Connect()
        {
            var window = WindowManager.SetWindow(new ConnectWindow(), new ConnectWindowViewModel());
            window.ShowDialog();
            if (!((ConnectWindowViewModel)window.ViewModel).IsCanceled)
            {
                IndexViewer.DataBase.Indices.Clear();
                _settings = ((ConnectWindowViewModel)window.ViewModel).Settings;
                ElesticHelper.GetInstance(_settings.Host);
                LoadUI();

            }
        }

        private void LoadUI()
        {

            IsWorking = true;
            Task.Factory.StartNew(
                () =>
                {
                    IndexViewer.DataBase.Indices = ElesticHelper.GetInstance().GetIndices(IndexViewer.DataBase);
                }).ContinueWith((tsk) =>
                {
                    if (tsk.Exception == null)
                    {
                        IsConnected = true;
                        QueryEditor.DataBase = IndexViewer.DataBase;
                    }
                    else
                    {
                        IsConnected = false;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            IndexViewer.DataBase.Indices.Clear();

                            MessageBox.Show("Connection to Elasticsearch failed", "ElasticShell");
                        });
                    }
                    IsWorking = false;
                });
        }
    }
}