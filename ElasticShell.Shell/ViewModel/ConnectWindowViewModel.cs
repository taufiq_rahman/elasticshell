﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Service.Model;
using Newtonsoft.Json;

namespace ElasticShell.Shell.ViewModel
{
    public sealed class ConnectWindowViewModel : BaseWindowViewModel
    {
        public ObservableCollection<Settings> ServerList
        {
            get { return _serverList; }
            set
            {
                _serverList = value;
                RaisePropertyChanged(() => ServerList);

            }
        }

        public Settings SelectedServer
        {
            get { return _selectedServer; }
            set
            {
                _selectedServer = value;
                if (_selectedServer != null)
                {
                    Host = _selectedServer.Host;
                }
                RaisePropertyChanged(() => SelectedServer);
            }
        }

        private ICommand _cancelCommand;
        private ICommand _okCommand;
        private ICommand _saveServerCommand;
        private ICommand _newServerCommand;
        private ICommand _deleteServerCommand;
        private Settings _selectedServer;
        private ObservableCollection<Settings> _serverList = new ObservableCollection<Settings>();
        private string _host;
        private string _port;

        public ConnectWindowViewModel()
        {
            IsCanceled = true;
            ServerList =new ObservableCollection<Settings>(JsonConvert.DeserializeObject<List<Settings>>(Properties.Settings.Default.ServerList));
            Settings = new Settings
            {
                Host = Properties.Settings.Default.Host,
            };

            Host = Settings.Host;
        }

        public bool IsCanceled { get; set; }
        public ICommand OkCommand => _okCommand ?? (_okCommand = new DelegateCommand(Ok));
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new DelegateCommand(Cancel));
        public ICommand SaveServerCommand => _saveServerCommand ?? (_saveServerCommand = new DelegateCommand(SaveServer));
        public ICommand DeleteServerCommand => _deleteServerCommand ?? (_deleteServerCommand = new DelegateCommand(DeleteServer));
        public ICommand NewServerCommand => _newServerCommand ?? (_newServerCommand = new DelegateCommand(NewServerServer));

        private void NewServerServer()
        {
            Settings = new Settings();
        }

        private void SaveServer()
        {
            try
            {
                new Uri(Host);
                Settings = new Settings
                {
                    Host = Host,
                };
                SelectedServer = null;
                ServerList.Add(Settings);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Host or port", "ElasticShell");
            }

        }


        private void DeleteServer()
        {
            if (SelectedServer != null)
            {
                Host = "";

                ServerList.Remove(SelectedServer);
                SelectedServer = null;
            }
        }

        public Settings Settings { get; set; }

        public string Host
        {
            get => _host;
            set
            {
                _host = value;
                RaisePropertyChanged(() => Host);
            }
        }

       
        private void Cancel()
        {
            IsCanceled = true;
            OnCloseWindow();
        }


        private void Ok()
        {
            IsCanceled = false;
            Host = Host.ToLower();
            Host = Host.Replace("http://", "");
            try
            {
                new Uri(Host);
                Settings.Host = Host;
                Properties.Settings.Default.Host = Settings.Host;
                Properties.Settings.Default.ServerList = JsonConvert.SerializeObject(ServerList);
                Properties.Settings.Default.Save();
                OnCloseWindow();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Host or port", "ElasticShell");
            }
        }
    }
}