﻿using ElasticShell.Service.Model;

namespace ElasticShell.Shell.View
{
    /// <summary>
    /// Interaction logic for IndexViewerView.xaml
    /// </summary>
    public partial class IndexViewerView : BaseUserControl
    {
        public IndexViewerView()
        {
            InitializeComponent();
        }
    }
}
