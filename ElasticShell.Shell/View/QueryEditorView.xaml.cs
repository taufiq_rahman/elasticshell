﻿using ElasticShell.Service.Model;

namespace ElasticShell.Shell.View
{
    /// <summary>
    /// Interaction logic for QueryEditorView.xaml
    /// </summary>
    public partial class QueryEditorView : BaseUserControl
    {
        public QueryEditorView()
        {
            InitializeComponent();
        }
    }
}
