﻿using System.ComponentModel;
using System.Windows;
using ElasticShell.Service.Model;

namespace ElasticShell.Shell.Window
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : BaseWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Application.Current.Shutdown();

            base.OnClosing(e);
        }
    }
}
