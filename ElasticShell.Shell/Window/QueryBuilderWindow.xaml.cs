﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ElasticShell.Service.Model;

namespace ElasticShell.Shell.Window
{
    /// <summary>
    /// Interaction logic for QueryBuilderWindow.xaml
    /// </summary>
    public partial class QueryBuilderWindow : BaseWindow
    {
        public QueryBuilderWindow()
        {
            InitializeComponent();
        }

        private void QueryBuilderWindow_OnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}
