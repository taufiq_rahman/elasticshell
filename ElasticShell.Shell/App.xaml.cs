﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace ElasticShell.Shell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        Bootstrap bootstrap = new Bootstrap();
        protected override void OnStartup(StartupEventArgs e)
        {

            AppDomain.CurrentDomain.AssemblyResolve += OnResolveAssembly;
            bootstrap.Start();
            base.OnStartup(e);
        }
        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs e)
        {
        
            var thisAssembly = Assembly.GetExecutingAssembly();
            var assemblyName = new AssemblyName(e.Name);
            var dllName = assemblyName.Name + ".dll";

            var resources = thisAssembly.GetManifestResourceNames().Where(s => s.EndsWith(dllName));
            if (resources.Any())
            {
  
                var resourceName = resources.First();
                using (var stream = thisAssembly.GetManifestResourceStream(resourceName))
                {
                    if (stream == null)
                    {
                        return null;
                    }
                    var block = new byte[stream.Length];
                    try
                    {
                        stream.Read(block, 0, block.Length);
                        return Assembly.Load(block);
                    }
                    catch (IOException)
                    {
                        return null;
                    }
                    catch (BadImageFormatException)
                    {
                        return null;
                    }
                }
            }
            return null;
        }
    }
}
