﻿using ElasticShell.Service.Model;
using ElasticShell.Shell.View;
using ElasticShell.Shell.ViewModel;
using MainWindow = ElasticShell.Shell.Window.MainWindow;

namespace ElasticShell.Shell
{

    public class Bootstrap
    {
        private BaseWindow _startUpWindow;
        private BaseWindowViewModel _startUpBaseWindowViewModel;
        public void Start()
        {
            _startUpWindow = new  MainWindow();
            _startUpBaseWindowViewModel= new MainWindowViewModel();

            _startUpWindow.DataContext = _startUpBaseWindowViewModel;
            _startUpWindow.Show();
        }

    }
}
