﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ElasticShell.Service.Model;
using Nest;
using Type = ElasticShell.Service.Model.Type;

namespace ElasticShell.Service.Helper
{
    public class ElesticHelper
    {
        public string ConnectionString { get; set; }
        private ConnectionSettings _connectionSettings;
        private ElasticClient _client;
        private static ElesticHelper _instance;

        public static ElesticHelper GetInstance()
        {
            return _instance ?? (_instance = new ElesticHelper());
        }

        public static ElesticHelper GetInstance(string connection)
        {

            return _instance = new ElesticHelper(connection);
        }
        private ElesticHelper()
        {

        }
        private ElesticHelper(string connection)
        {
            ConnectionString = connection;
            _connectionSettings = new ConnectionSettings(new Uri(connection));
            _client = new ElasticClient(_connectionSettings);
        }

        public List<Index> GetIndices()
        {
            List<Index> indices = new List<Index>();
            var ind = _client.CatIndices();
            foreach (var catIndicesRecord in ind.Records.OrderBy(q => q.Index))
            {
                var index = new Index() { Name = catIndicesRecord.Index };
                //var types = _client.Search<dynamic>(i => i.AllTypes().Index(index.Name).Source(sou => sou.Include(rr => rr.Fields(new[] { "_type" }))));
                var types = _client.GetMapping<dynamic>(qq => qq.AllTypes().Index(index.Name));
                foreach (var mapping in types.IndexTypeMappings.OrderBy(q => q.Key.Name))
                {
                    foreach (var type in mapping.Value)
                    {
                        int count = int.Parse(_client.Count<dynamic>(qq => qq.Index(index.Name).Type(type.Key.Name)).Count.ToString());
                        index.Add(new Model.Type() { Name = type.Key.Name.ToLower(), DocumentCount = count });
                    }
                }
                indices.Add(index);
            }
            return indices;
        }
        public object Raw(string command, string queryText)
        {

            try
            {
                HttpClient httpClient = new HttpClient();
                var opIndex = command.IndexOf(' ');
                var oparation = command.Substring(0, opIndex).Trim();
                var parms = command.Substring(opIndex, command.Length - opIndex).Trim();
                var url = $"{ConnectionString}/{parms}";
                if ( !command.ToLower().Contains("/_mapping") && !string.IsNullOrEmpty(queryText))
                {
                    var task = httpClient.PostAsync(url, new StringContent(queryText));
                    task.Wait();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject(task.Result.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    Task<HttpResponseMessage> task;
                    switch (oparation.ToLower())
                    {
                        case "get":
                            task = httpClient.GetAsync(url);
                            task.Wait();

                            return Newtonsoft.Json.JsonConvert.DeserializeObject(task.Result.Content.ReadAsStringAsync().Result);
                        case "delete":
                            task = httpClient.DeleteAsync(url);
                            task.Wait();
                            return Newtonsoft.Json.JsonConvert.DeserializeObject(task.Result.Content.ReadAsStringAsync().Result);
                        default:
                            return null;
                    }

                }
            }
            catch (Exception)
            {
                return null;
            }
            

        }
        public object Raw(string index, string type, string queryText)
        {
            if (string.IsNullOrEmpty(queryText)) return null;
            HttpClient httpClient = new HttpClient();
            var url = $"{ConnectionString}/{index}/{type}/_search";
            var task = httpClient.PostAsync(url, new StringContent(queryText));
            task.Wait();
            return Newtonsoft.Json.JsonConvert.DeserializeObject(task.Result.Content.ReadAsStringAsync().Result);
        }
        public object Mapping(string index, string type)
        {
            HttpClient httpClient = new HttpClient();
            var url = $"{ConnectionString}/{index}/{type}/_mapping";
            var task = httpClient.GetAsync(url);
            task.Wait();
            return Newtonsoft.Json.JsonConvert.DeserializeObject(task.Result.Content.ReadAsStringAsync().Result);
        }
        public object Search(int docType, string index, string type, int from, int size, string queryText = null, string sort = null)
        {
            ISearchResponse<dynamic> res;
            if (queryText == null)
            {
                if (!string.IsNullOrEmpty(sort))
                {
                    res = _client.Search<dynamic>(q => q.Index(index).Type(type).From(from).Size(size).Sort(r => r.Ascending(sort)));
                }
                else
                {

                    res = _client.Search<dynamic>(q => q.Index(index).Type(type).From(from).Size(size));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(sort))
                {
                    res = _client.Search<dynamic>(
                            q => q.Index(index).Type(type).From(from).Size(size).Query(qr => qr.Raw(queryText)).Sort(r => r.Ascending(sort)));
                }
                else
                {
                    res = _client.Search<dynamic>(
                                q => q.Index(index).Type(type).From(from).Size(size).Query(qr => qr.Raw(queryText)));

                }
            }
            if (docType == 0)
                return res.Documents.ToList();
            return res.Hits.Select(q => new { _index = q.Index, _type = q.Type, _id = q.Id, _score = q.Score, _source = q.Source, }).ToList();
        }
        public void DeteteByIndex(string index)
        {
            _client.DeleteIndex(index);
        }

        public ObservableCollection<Index> GetIndices(DataBase dataBase)
        {

            var ind = _client.CatIndices();
            try
            {
                foreach (var catIndicesRecord in ind.Records.OrderBy(q => q.Index))
                {
                    var indpre = dataBase.Indices.FirstOrDefault(q => q.Name == catIndicesRecord.Index);
                    var index = indpre ??
                                new Index() { Name = catIndicesRecord.Index };
                    //var types = _client.Search<dynamic>(i => i.AllTypes().Index(index.Name).Source(sou => sou.Include(rr => rr.Fields(new[] { "_type" }))));
                    var types = _client.GetMapping<dynamic>(qq => qq.AllTypes().Index(index.Name));
                    foreach (var mapping in types.IndexTypeMappings.OrderBy(q => q.Key))
                    {
                        foreach (var type in mapping.Value.OrderBy(q => q.Key.Name))
                        {
                            int count =
                                int.Parse(
                                    _client.Count<dynamic>(qq => qq.Index(index.Name).Type(type.Key.Name))
                                        .Count.ToString());
                            var typePre = index.Types.FirstOrDefault(q => q.Name == type.Key.Name);
                            var t = typePre ?? new Type();
                            t.Name = type.Key.Name;
                            t.DocumentCount = count;
                            if (typePre == null)
                                index.Add(t);
                        }
                    }
                    if (indpre == null)
                        dataBase.Add(index);
                }
                var rdd = dataBase.Indices.Where(q => ind.Records.All(r => r.Index != q.Name)).ToList();
                foreach (var index in rdd)
                {

                    dataBase.Remove(index);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dataBase.Indices;
        }

        public void DeteteByType(string index, string type)
        {
            int count = int.Parse(_client.Count<dynamic>(qq => qq.Index(index).Type(type)).Count.ToString());
            var ind = _client.Search<dynamic>(qq => qq.Index(index).Type(type).From(0).Size(count));
            foreach (var hit in ind.Hits)
            {
                var fff = _client.Delete<dynamic>(hit.Id, q => q.Type(type).Index(index));
            }

        }

        public bool Add(string index, string type, string id, object obj)
        {
            var res = _client.Index(obj, q => q.Index(index).Type(type).Id(id));
            return res.Created;
        }
        public bool Delete(string index, string type, string id)
        {
            return _client.Delete<dynamic>(id, q => q.Index(index).Type(type)).Found;
        }

        public List<string> MappingPropertyNames(string index, string type)
        {
            List<string> result = new List<string>();
            var properties = _client.GetMapping<dynamic>(q => q.Type(type).Index(index)).Mapping.Properties;
            GetProperties(properties, result);
            return result;
        }

        private static void GetProperties(IProperties properties, List<string> result, string prefix = null)
        {
            if (properties != null)
                foreach (var property in properties)
                {
                    if (property.Value.Type.Name == "object")
                    {
                        GetProperties(((ObjectProperty)property.Value).Properties, result,
                            (prefix == null ? "" : prefix + ".") + property.Key.Name);
                    }
                    else
                    {
                        result.Add((prefix == null ? "" : prefix + ".") + property.Key.Name);

                    }
                }
        }
    }
}
