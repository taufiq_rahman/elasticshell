﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ElasticShell.Interface.Interface;
using ElasticShell.Service.Model;

namespace ElasticShell.Service.Helper
{
    public static class WindowManager
    {
        public struct WindowObject
        {
            public BaseWindow Window { get; set; }
            public BaseWindowViewModel ViewModel { get; set; }

            public WindowObject(BaseWindow window, BaseWindowViewModel viewModel)
            {
                Window = window;
                ViewModel = viewModel;
            }

            public void Show()
            {
                try
                {
                    this.Window.Show();

                }
                catch (Exception)
                { 
                }
            }

            public void ShowDialog()
            {
                Window.WindowStartupLocation= WindowStartupLocation.CenterScreen;
               var d= this.Window.ShowDialog();
            }
        }
        public static WindowObject SetWindow(BaseWindow window, BaseWindowViewModel viewModel)
        {
            viewModel.Window = window;
            return SetWindow(new WindowObject(viewModel.Window, viewModel));
        }
        public static WindowObject SetWindow(WindowObject windowObj)
        {
            windowObj.Window.DataContext = windowObj.ViewModel;
            windowObj.ViewModel.CloseWindow += CloseWindow;
            return windowObj;
        }

        private static void CloseWindow(BaseWindow window)
        {
            window.Close();
        }
    }
}
