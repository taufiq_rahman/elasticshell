﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticShell.Service.Helper
{
    public static class MessageHelper
    {
        public delegate void IsBusyHandler(bool busy);

        public static event IsBusyHandler Busy;

        public delegate void ReloadIndexHandler();

        public static event ReloadIndexHandler ReloadIndex;

        public static void SetBusy(bool isBusy)
        {
            OnBusy(isBusy);
        }

        public static void Reload()
        {
            OnReloadIndex();
        }
        private static void OnBusy(bool busy)
        {
            Busy?.Invoke(busy);
        }

        private static void OnReloadIndex()
        {
            ReloadIndex?.Invoke();

        }
    }
}
