﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ElasticShell.Interface.Interface;
namespace ElasticShell.Service.Model
{
    public abstract class BaseWindow: Window, IBaseWindow
    {
        public void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");

            e.Handled = regex.IsMatch(e.Text);
        
        }
    }
}
