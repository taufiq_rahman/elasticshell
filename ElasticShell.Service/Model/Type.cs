using System.Collections.ObjectModel;
using System.Windows.Media;
using ElasticShell.Command;

namespace ElasticShell.Service.Model
{
    public class Type : NotifyPropertyChanged
    {
        private bool _isSelected;
        private bool _isExpanded;
        private int _documentCount;
        private SolidColorBrush _highlightColor;
        private bool _isHighlighted;
        public string Name { get; set; }

        public int DocumentCount
        {
            get { return _documentCount; }
            set
            {
                _documentCount = value;
                RaisePropertyChanged(() => DocumentCount);

            }
        }

        public delegate void TypeSelectedHandler(Type type);
        public event TypeSelectedHandler TypeSelected;


        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                RaisePropertyChanged(() => IsExpanded);
            }
        }
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                RaisePropertyChanged(() => IsSelected);

                if (value) OnTypeSelected(this);
            }
        }

        public SolidColorBrush HighlightColor
        {
            get
            {
                return _highlightColor;
                
            }
            set
            {
                _highlightColor = value;
                RaisePropertyChanged(() => HighlightColor);
            }
        }

        public bool IsHighlighted
        {
            get { return _isHighlighted; }
            set
            {
                HighlightColor = value ? Brushes.LightSkyBlue : Brushes.Transparent;
                _isHighlighted = value;
            }
        }

        protected virtual void OnTypeSelected(Type type)
        {
            TypeSelected?.Invoke(type);
        }
    }

}