using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using ElasticShell.Command;

namespace ElasticShell.Service.Model
{
    public class Index:NotifyPropertyChanged
    {
        private bool _isSelected;
        private bool _isExpanded;
        private SolidColorBrush _highlightColor;
        private bool _isHighlighted;

        public delegate void TypeSelectedHandler(Index index,Type type);
        public event TypeSelectedHandler TypeSelected;
        public Index()
        {
            this.Types = new ObservableCollection<Type>();
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value; 
                RaisePropertyChanged(()=> IsExpanded);
            }
        }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                RaisePropertyChanged(() => IsExpanded);
                if (value)
                    OnTypeSelected(null);
            }
        }


        public string Name { get; set; }

        public ObservableCollection<Type> Types { get; set; }

        public void Add(Type type)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                type.TypeSelected += Type_TypeSelected;
                Types.Add(type);

            });
    
        }
        public SolidColorBrush HighlightColor
        {
            get
            {
                return _highlightColor;

            }
            set
            {
                _highlightColor = value;
                RaisePropertyChanged(() => HighlightColor);
            }
        }

        public bool IsHighlighted
        {
            get { return _isHighlighted; }
            set
            {
                HighlightColor = value ? Brushes.LightSkyBlue : Brushes.Transparent;
                _isHighlighted = value;
            }
        }

        private void Type_TypeSelected(Type type)
        {
            OnTypeSelected(type);
        }

        protected virtual void OnTypeSelected(Type type)
        {
            TypeSelected?.Invoke(this,type);
        }
    }
}