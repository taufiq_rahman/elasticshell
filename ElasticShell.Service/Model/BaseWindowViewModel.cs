﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticShell.Command;
using ElasticShell.Interface.Interface;
using ElasticShell.Service.Helper;

namespace ElasticShell.Service.Model
{
    public abstract class BaseWindowViewModel : NotifyPropertyChanged, IBaseWindowViewModel
    {
        private bool _isWorking;

        public bool IsWorking
        {
            get { return _isWorking; }
            set
            {
                _isWorking = value;
                MessageHelper.SetBusy(value);
                RaisePropertyChanged(() => IsWorking);
            }
        }

        public BaseWindow Window { get; set; }

        public delegate void CloseWindowEventHandel(BaseWindow window);
        public event CloseWindowEventHandel CloseWindow;

        protected virtual void SetBaseWindowViewModel(BaseWindow window)
        {
            Window = window;
        }

        protected virtual void OnCloseWindow()
        {
            CloseWindow?.Invoke(Window);
        }
    }
}
