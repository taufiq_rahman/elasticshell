﻿namespace ElasticShell.Service.Model
{
    public class Settings
    {
        private string _displayText;

        public Settings()
        {
            
        }


        public string Host { get; set; }

        public string DisplayText => Host;
    }
}
