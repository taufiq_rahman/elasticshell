﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ElasticShell.Service.Model
{
    public class DataBase
    {
        public delegate void TypeSelectedHandler(DataBase database,Index index,Type type);
        public event TypeSelectedHandler TypeSelected;
        public string Name { get; set; }
        public ObservableCollection<Index> Indices { get; set; }

        public DataBase()
        {
            Indices= new ObservableCollection<Index>();
        }
        public void Add(Index index)
        {
            index.TypeSelected += Index_TypeSelected;
            Application.Current.Dispatcher.Invoke(() =>
            {
                this.Indices.Add(index);

            });
        }

        private void Index_TypeSelected(Index index,Type type)
        {
            OnTypeSelected(index,type);
        }

        protected virtual void OnTypeSelected(Index index, Type type)
        {
            TypeSelected?.Invoke(this,index,type);
        }

        public void Remove(Index index)
        {
            index.TypeSelected += Index_TypeSelected;
            Application.Current.Dispatcher.Invoke(() =>
            {
                this.Indices.Remove(index);

            });
        }
    }
}
