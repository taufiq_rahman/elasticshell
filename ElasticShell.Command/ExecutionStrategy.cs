﻿using System;

namespace ElasticShell.Command
{
    #region

    

    #endregion

    /// <summary>
    ///     Defines the interface for a strategy of execution for the CommandBehaviorBinding
    /// </summary>
    public interface IExecutionStrategy
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the Behavior that we execute this strategy
        /// </summary>
        CommandBehaviorBinding Behavior { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Executes according to the strategy type
        /// </summary>
        /// <param name="parameter">
        ///     The parameter to be used in the execution
        /// </param>
        void Execute(object parameter);

        #endregion
    }

    /// <summary>
    ///     Executes a command
    /// </summary>
    public class CommandExecutionStrategy : IExecutionStrategy
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the Behavior that we execute this strategy
        /// </summary>
        public CommandBehaviorBinding Behavior { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Executes the Command that is stored in the CommandProperty of the CommandExecution
        /// </summary>
        /// <param name="parameter">
        ///     The parameter for the command
        /// </param>
        public void Execute(object parameter)
        {
            if (this.Behavior == null)
            {
                throw new InvalidOperationException("Behavior property cannot be null when executing a strategy");
            }

            if (this.Behavior.Command.CanExecute(this.Behavior.CommandParameter))
            {
                this.Behavior.Command.Execute(this.Behavior.CommandParameter);
            }
        }

        #endregion
    }
}