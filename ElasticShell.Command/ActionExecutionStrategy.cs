﻿namespace ElasticShell.Command
{
    /// <summary>
    ///     executes a delegate
    /// </summary>
    public class ActionExecutionStrategy : IExecutionStrategy
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the Behavior that we execute this strategy
        /// </summary>
        public CommandBehaviorBinding Behavior { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Executes an Action delegate
        /// </summary>
        /// <param name="parameter">
        ///     The parameter to pass to the Action
        /// </param>
        public void Execute(object parameter)
        {
            this.Behavior.Action(parameter);
        }

        #endregion
    }
}