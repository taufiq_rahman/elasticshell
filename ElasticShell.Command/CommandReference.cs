﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace ElasticShell.Command
{
    #region

    

    #endregion

    public class CommandReference : Freezable, ICommand
    {
        #region Static Fields

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command", 
            typeof(ICommand), 
            typeof(CommandReference), 
            new PropertyMetadata(OnCommandChanged));

        #endregion

        #region Fields

        private List<WeakReference> canExecuteChangedHandlers;

        #endregion

        #region Public Events

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                CommandManagerHelper.AddWeakReferenceHandler(ref this.canExecuteChangedHandlers, value, 2);
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
                CommandManagerHelper.RemoveWeakReferenceHandler(this.canExecuteChangedHandlers, value);
            }
        }

        #endregion

        #region Public Properties

        public ICommand Command
        {
            get
            {
                return (ICommand)this.GetValue(CommandProperty);
            }

            set
            {
                this.SetValue(CommandProperty, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        public bool CanExecute(object parameter)
        {
            return this.Command != null && this.Command.CanExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.Command.Execute(parameter);
        }

        #endregion

        #region Methods

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var commandReference = d as CommandReference;
            var oldCommand = e.OldValue as ICommand;
            if (oldCommand == null)
            {
                return;
            }

            if (commandReference != null)
            {
                CommandManagerHelper.RemoveHandlersFromRequerySuggested(commandReference.canExecuteChangedHandlers);
            }
        }

        #endregion
    }
}