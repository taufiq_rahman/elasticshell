﻿using System;
using System.Windows;
using System.Windows.Input;

namespace ElasticShell.Command
{
    #region

    

    #endregion

    public class BehaviorBinding : Freezable
    {
        #region Static Fields

        public static readonly DependencyProperty ActionProperty = DependencyProperty.Register(
            "Action", 
            typeof(Action<object>), 
            typeof(BehaviorBinding), 
            new FrameworkPropertyMetadata(null, OnActionChanged));

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register(
                "CommandParameter", 
                typeof(object), 
                typeof(BehaviorBinding), 
                new FrameworkPropertyMetadata(null, OnCommandParameterChanged));

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command", 
            typeof(ICommand), 
            typeof(BehaviorBinding), 
            new FrameworkPropertyMetadata(null, OnCommandChanged));

        public static readonly DependencyProperty EventProperty = DependencyProperty.Register(
            "Event", 
            typeof(string), 
            typeof(BehaviorBinding), 
            new FrameworkPropertyMetadata(null, OnEventChanged));

        #endregion

        #region Fields

        private CommandBehaviorBinding behavior;

        private DependencyObject owner;

        #endregion

        #region Public Properties

        public Action<object> Action
        {
            get
            {
                return (Action<object>)this.GetValue(ActionProperty);
            }

            set
            {
                this.SetValue(ActionProperty, value);
            }
        }

        public ICommand Command
        {
            get
            {
                return (ICommand)this.GetValue(CommandProperty);
            }

            set
            {
                this.SetValue(CommandProperty, value);
            }
        }

        public object CommandParameter
        {
            get
            {
                return this.GetValue(CommandParameterProperty);
            }

            set
            {
                this.SetValue(CommandParameterProperty, value);
            }
        }

        public string Event
        {
            get
            {
                return (string)this.GetValue(EventProperty);
            }

            set
            {
                this.SetValue(EventProperty, value);
            }
        }

        public DependencyObject Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                this.owner = value;
                this.ResetEventBinding();
            }
        }

        #endregion

        #region Properties

        internal CommandBehaviorBinding Behavior
        {
            get
            {
                if (this.behavior == null)
                {
                    this.behavior = new CommandBehaviorBinding();
                }

                return this.behavior;
            }
        }

        #endregion

        #region Methods

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        protected virtual void OnActionChanged(DependencyPropertyChangedEventArgs e)
        {
            this.Behavior.Action = this.Action;
        }

        protected virtual void OnCommandChanged(DependencyPropertyChangedEventArgs e)
        {
            this.Behavior.Command = this.Command;
        }

        protected virtual void OnCommandParameterChanged(DependencyPropertyChangedEventArgs e)
        {
            this.Behavior.CommandParameter = this.CommandParameter;
        }

        protected virtual void OnEventChanged(DependencyPropertyChangedEventArgs e)
        {
            this.ResetEventBinding();
        }

        private static void OnActionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BehaviorBinding)d).OnActionChanged(e);
        }

        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BehaviorBinding)d).OnCommandChanged(e);
        }

        private static void OnCommandParameterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BehaviorBinding)d).OnCommandParameterChanged(e);
        }

        private static void OnEventChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BehaviorBinding)d).OnEventChanged(e);
        }

        private static void OwnerReset(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BehaviorBinding)d).ResetEventBinding();
        }

        private void ResetEventBinding()
        {
            if (this.Owner != null)
            {
                // only do this when the Owner is set
                // check if the Event is set. If yes we need to rebind the Command to the new event and unregister the old one
                if (this.Behavior.Event != null && this.Behavior.Owner != null)
                {
                    this.Behavior.Dispose();
                }

                // bind the new event to the command
                this.Behavior.BindEvent(this.Owner, this.Event);
            }
        }

        #endregion
    }
}