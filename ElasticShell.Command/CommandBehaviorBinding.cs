﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace ElasticShell.Command
{
    #region

    

    #endregion

    public class CommandBehaviorBinding : IDisposable
    {
        #region Fields

        private Action<object> action;

        private ICommand command;

        private bool disposed;

        private IExecutionStrategy strategy;

        #endregion

        #region Public Properties

        public Action<object> Action
        {
            get
            {
                return this.action;
            }

            set
            {
                this.action = value;

                // set the execution strategy to execute the action
                this.strategy = new ActionExecutionStrategy { Behavior = this };
            }
        }

        public ICommand Command
        {
            get
            {
                return this.command;
            }

            set
            {
                this.command = value;

                // set the execution strategy to execute the command
                this.strategy = new CommandExecutionStrategy { Behavior = this };
            }
        }

        public object CommandParameter { get; set; }

        public EventInfo Event { get; private set; }

        public Delegate EventHandler { get; private set; }

        public string EventName { get; private set; }

        public DependencyObject Owner { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void BindEvent(DependencyObject owner, string eventName)
        {
            if (string.IsNullOrEmpty(eventName))
            {
                return;
            }

            this.EventName = eventName;
            this.Owner = owner;
            this.Event = this.Owner.GetType().GetEvent(this.EventName, BindingFlags.Public | BindingFlags.Instance);
            if (this.Event == null)
            {
                throw new InvalidOperationException(string.Format("Could not resolve event name {0}", this.EventName));
            }

            // Create an event handler for the event that will call the ExecuteCommand method
            this.EventHandler = EventHandlerGenerator.CreateDelegate(
                this.Event.EventHandlerType, 
                typeof(CommandBehaviorBinding).GetMethod("Execute", BindingFlags.Public | BindingFlags.Instance), 
                this);

            // Register the handler to the Event
            this.Event.AddEventHandler(this.Owner, this.EventHandler);
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.Event.RemoveEventHandler(this.Owner, this.EventHandler);
                this.disposed = true;
            }
        }

        public void Execute()
        {
            if (this.strategy != null)
            {
                this.strategy.Execute(this.CommandParameter);
            }
        }

        #endregion
    }
}