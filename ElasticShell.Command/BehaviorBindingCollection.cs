using System.Windows;

namespace ElasticShell.Command
{
    #region

    

    #endregion

    /// <summary>
    ///     Collection to store the list of behaviors. This is done so that you can initiate it from XAML
    ///     This inherits from freezable so that it gets inheritance context for DataBinding to work
    /// </summary>
    public class BehaviorBindingCollection : FreezableCollection<BehaviorBinding>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the Owner of the binding
        /// </summary>
        public DependencyObject Owner { get; set; }

        #endregion
    }
}