﻿using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Service.Model;
using Newtonsoft.Json;

namespace EsasticShell.MiniShell.ViewModel
{
    public class DocumentViewerViewModel : BaseWindowViewModel
    {
        private string _documentString ="";
        private Visibility _textDocumantVisibility = Visibility.Hidden;
        private bool _isDocumentView = false;
        private bool _showIndexNumber;
        private Visibility _showIndexNumberVisibility;
        private ICommand _copyCommand;
        private string _treeSelectedItem = "";
        private string _textboxSelectedItem = "";
        private string _searchText="";
        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new DelegateCommand(Copy));

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                RaisePropertyChanged(() => SearchText);
            }
        }

        public DocumentViewerViewModel()
        {
            
        }
        private void Copy()
        {
            //if (IsDocumentView)
            //{
            //    Clipboard.SetText(TextboxSelectedItem);
            //}
            //else
            //{
            //    Clipboard.SetText(TreeSelectedItem);

            //}
        }

        public bool ShowIndexNumber
        {
            get { return _showIndexNumber; }
            set
            {
                _showIndexNumber = value;
                RaisePropertyChanged(() => ShowIndexNumber);
            }
        }


        public string TreeSelectedItem
        {
            get
            {
                return _treeSelectedItem;
            }
            set
            {
                _treeSelectedItem = value;
                RaisePropertyChanged(() => TreeSelectedItem);
            }
        }

        public string TextboxSelectedItem
        {
            get { return _textboxSelectedItem; }
            set
            {
                _textboxSelectedItem = value;
                RaisePropertyChanged(() => TextboxSelectedItem);
            }
        }
        public string DocumentString
        {
            get { return _documentString; }
            set
            {
                _documentString = value;
                RaisePropertyChanged(() => DocumentString);
            }
        }

        public Visibility ShowIndexNumberVisibility
        {
            get { return _showIndexNumberVisibility; }
            set
            {
                _showIndexNumberVisibility = value;
                RaisePropertyChanged(() => ShowIndexNumberVisibility);

            }
        }

        public Visibility TextDocumantVisibility
        {
            get
            {
                return _textDocumantVisibility;
            }
            set
            {
                _textDocumantVisibility = value;

                RaisePropertyChanged(() => TextDocumantVisibility);
            }
        }

        public bool IsDocumentView
        {
            get { return _isDocumentView; }
            set
            {
                _isDocumentView = value;
                if (value)
                {
                    TextDocumantVisibility = Visibility.Visible;
                    ShowIndexNumberVisibility = Visibility.Hidden;
                }
                else
                {
                    TextDocumantVisibility = Visibility.Hidden;
                    ShowIndexNumberVisibility = Visibility.Visible;

                }
                RaisePropertyChanged(() => IsDocumentView);

            }
        }
        public bool IsTreeView
        {
            get { return !_isDocumentView; }
            set
            {
                _isDocumentView = !value;
                RaisePropertyChanged(() => IsTreeView);

            }
        }

        public void DisplayResult(object result)
        {
            DocumentString = JsonConvert.SerializeObject(result, Formatting.Indented);
        }
    }
}
