﻿using System.Collections.ObjectModel;
using System.Windows.Controls;
using ElasticShell.Service.Model;

namespace EsasticShell.MiniShell.ViewModel
{
    public class QueryBuilderWindowViewModel : BaseWindowViewModel
    {
        public delegate void ChangeQueryHandler(string query);

        private DataBase _dataBase;

        public QueryBuilderWindowViewModel()
        {
            QueryBuilderViewModel = new QueryBuilderViewModel();
        }

        public ObservableCollection<TabItem> Tabs { get; set; }


        public QueryBuilderViewModel QueryBuilderViewModel { get; set; }

        public DataBase DataBase
        {
            get { return _dataBase; }
            set
            {
                _dataBase = value;
                QueryBuilderViewModel.DataBase = _dataBase;
            }
        }

        public event ChangeQueryHandler ChangeQuery;

        protected virtual void OnChangeQuery(string query)
        {
            ChangeQuery?.Invoke(query);
        }


        public void IndexTypeChanged(Index selectedIndex, Type selectedType)
        {
            QueryBuilderViewModel.SetIndex(selectedIndex, selectedType);
        }
    }
}