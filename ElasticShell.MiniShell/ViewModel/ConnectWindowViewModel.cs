﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using ElasticShell.Command;
using ElasticShell.Service.Model;
using Newtonsoft.Json;

namespace EsasticShell.MiniShell.ViewModel
{
    public sealed class ConnectWindowViewModel : BaseWindowViewModel
    {
        public ObservableCollection<Settings> ServerList
        {
            get { return _serverList; }
            set
            {
                _serverList = value;
                RaisePropertyChanged(() => ServerList);

            }
        }

        public Settings SelectedServer
        {
            get { return _selectedServer; }
            set
            {
                _selectedServer = value;
                if (_selectedServer != null)
                {
                    Ip = _selectedServer.Host;
                    Port = _selectedServer.Port;
                }
                RaisePropertyChanged(() => SelectedServer);
            }
        }

        private ICommand _cancelCommand;
        private ICommand _okCommand;
        private ICommand _saveServerCommand;
        private ICommand _newServerCommand;
        private ICommand _deleteServerCommand;
        private Settings _selectedServer;
        private ObservableCollection<Settings> _serverList = new ObservableCollection<Settings>();
        private string _ip;
        private string _port;

        public ConnectWindowViewModel()
        {
            IsCanceled = true;
            ServerList =new ObservableCollection<Settings>(JsonConvert.DeserializeObject<List<Settings>>(ElasticShell.Shell.Properties.Settings.Default.ServerList));
            Settings = new Settings
            {
                Host = ElasticShell.Shell.Properties.Settings.Default.Ip,
                Port = ElasticShell.Shell.Properties.Settings.Default.Port
            };

            Ip = Settings.Host;
            Port = Settings.Port;
        }

        public bool IsCanceled { get; set; }
        public ICommand OkCommand => _okCommand ?? (_okCommand = new DelegateCommand(Ok));
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new DelegateCommand(Cancel));
        public ICommand SaveServerCommand => _saveServerCommand ?? (_saveServerCommand = new DelegateCommand(SaveServer));
        public ICommand DeleteServerCommand => _deleteServerCommand ?? (_deleteServerCommand = new DelegateCommand(DeleteServer));
        public ICommand NewServerCommand => _newServerCommand ?? (_newServerCommand = new DelegateCommand(NewServerServer));

        private void NewServerServer()
        {
            Settings = new Settings();
        }

        private void SaveServer()
        {
            try
            {
                new Uri($"http://{Ip}:{Port}");
                Settings = new Settings
                {
                    Host = Ip,
                    Port = Port
                };
                SelectedServer = null;
                ServerList.Add(Settings);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Host or port", "ElasticShell");
            }

        }


        private void DeleteServer()
        {
            if (SelectedServer != null)
            {
                Ip = "";
                Port = "";

                ServerList.Remove(SelectedServer);
                SelectedServer = null;
            }
        }

        public Settings Settings { get; set; }

        public string Ip
        {
            get { return _ip; }
            set
            {
                _ip = value;
                RaisePropertyChanged(() => Ip);
            }
        }

        public string Port
        {
            get { return _port; }
            set
            {
                _port = value;
                RaisePropertyChanged(() => Port);
            }
        }

        private void Cancel()
        {
            IsCanceled = true;
            OnCloseWindow();
        }


        private void Ok()
        {
            IsCanceled = false;
            Ip = Ip.ToLower();
            Ip = Ip.Replace("http://", "");
            try
            {
                new Uri($"http://{Ip}:{Port}");
                Settings.Host = Ip;
                Settings.Port = Port;
                ElasticShell.Shell.Properties.Settings.Default.Ip = Settings.Host;
                ElasticShell.Shell.Properties.Settings.Default.Port = Settings.Port;
                ElasticShell.Shell.Properties.Settings.Default.ServerList = JsonConvert.SerializeObject(ServerList);
                ElasticShell.Shell.Properties.Settings.Default.Save();
                OnCloseWindow();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Host or port", "ElasticShell");
            }
        }
    }
}