using System;
using System.Windows.Controls;
using System.Windows.Input;
using Newtonsoft.Json;

namespace ElasticShell.Components
{
    internal class Tab
    {
        public string Code { get; set; }

        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                TabItem.Header = value;
            }
        }

        public bool IsLoaded { get; set; }

        public delegate void RemovedTabHandler(Tab sender);

        public delegate void TabSelectedHandler(Tab sender);

        public delegate void TextChangedHandler(Tab sender, string text);

        private string _text;
        private string _header;

        public Tab()
        {
            Code = Guid.NewGuid().ToString().Replace("-","");
            TabItem = new TabItem();
            Header = "New Tab";
            TabItem.MouseDoubleClick += TabItem_MouseDoubleClick;
        }

        public Tab(string header)
        {
            Code = Guid.NewGuid().ToString().Replace("-", "");
        
            TabItem = new TabItem();
            Header = header;
            TabItem.MouseDoubleClick += TabItem_MouseDoubleClick;
            TabItem.MouseDown += TabItem_MouseDown;
        }

        [JsonIgnore]
        public TabItem TabItem { get; set; }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnTextChanged(_text);
            }
        }

        public event RemovedTabHandler RemoveTab;

        public event TabSelectedHandler TabSelected;

        public event TextChangedHandler TextChanged;

        private void TabItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            OnTabSelected();
        }

        private void TabItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnRemoveTab(this);
        }

        protected virtual void OnRemoveTab(Tab sender)
        {
            RemoveTab?.Invoke(sender);
        }

        protected virtual void OnTabSelected()
        {
            TabSelected?.Invoke(this);
        }

        protected virtual void OnTextChanged(string text)
        {
            TextChanged?.Invoke(this, text);
        }
    }
}