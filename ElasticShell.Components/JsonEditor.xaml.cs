﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using AurelienRibon.Ui.SyntaxHighlightBox;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Brushes = System.Windows.Media.Brushes;
using Point = System.Windows.Point;
using Size = System.Windows.Size;


namespace ElasticShell.Components
{
    /// <summary>
    ///     Interaction logic for JsonEditor.xaml
    /// </summary>
    public partial class JsonEditor : UserControl
    {
        public delegate string LoadTabsHandler();


        public static readonly DependencyProperty JsonStringDependencyProperty =
            DependencyProperty.Register(
                "DocumentString",
                typeof(string),
                typeof(JsonEditor),
                new PropertyMetadata(DocumentStringChanged));

        public static readonly DependencyProperty IndentDependencyProperty =
            DependencyProperty.Register(
                "Indent",
                typeof(bool),
                typeof(JsonEditor),
                new PropertyMetadata(IndentChanged));

        public static readonly DependencyProperty LoadTabsDataDependencyProperty =
            DependencyProperty.Register(
                "LoadTabsData",
                typeof(string),
                typeof(JsonEditor),
                new PropertyMetadata(LoadTabsDataChanged));

        public static readonly DependencyProperty SaveDataDependencyProperty =
            DependencyProperty.Register(
                "SaveTabData",
                typeof(string),
                typeof(JsonEditor),
                new PropertyMetadata(SaveDataChanged));

        public static readonly DependencyProperty SelectedTabCodeDataDependencyProperty =
            DependencyProperty.Register(
                "SelectedTabCode",
                typeof(string),
                typeof(JsonEditor),
                new PropertyMetadata(SelectedTabCodeChanged));

        public static readonly DependencyProperty QueryDependencyProperty =
            DependencyProperty.Register(
                "Query",
                typeof(Query),
                typeof(JsonEditor),
                new PropertyMetadata(QueryChanged));

        public static readonly DependencyProperty DefaultQueryStringDependencyProperty =
    DependencyProperty.Register(
        "DefaultQueryString",
        typeof(string),
        typeof(JsonEditor),
        new PropertyMetadata(DefaultQueryStringChanged));

        private readonly TabControl _tabControl;
        private readonly Timer _timer = new Timer(100);
        private bool _textLoading;

        public class Highlighter : IHighlighter
        {

            public int Highlight(FormattedText text, int previousBlockCode)
            {
                var brackets = GetBracketModels(text.Text);
                foreach (var bracketModel in brackets)
                {

                    text.SetForegroundBrush(Brushes.Blue, bracketModel.Value.OpenBracket, 1);
                    if (bracketModel.Value.CloseBracket != -1)
                        text.SetForegroundBrush(Brushes.Blue, bracketModel.Value.CloseBracket, 1);
                }
                return previousBlockCode;
            }

            private int FindOpenIndex(string text, int i)
            {
                var firstopenIndex = text.IndexOf("{", i, StringComparison.Ordinal);
                var secondopenIndex = text.IndexOf("[", i, StringComparison.Ordinal);
                return firstopenIndex < secondopenIndex || secondopenIndex == -1 ? firstopenIndex : secondopenIndex;
            }

            private int FindCloseIndex(string text, int i)
            {
                var firstCloseIndex = text.IndexOf("}", i, StringComparison.Ordinal);
                var secondCloseIndex = text.IndexOf("]", i, StringComparison.Ordinal);
                return firstCloseIndex < secondCloseIndex || secondCloseIndex == -1 ? firstCloseIndex : secondCloseIndex;
            }

            private Dictionary<int, BracketModel> GetBracketModels(string text)
            {
                var x = 0;
                var brackets = new Dictionary<int, BracketModel>();
                for (var i = 0; i < text.Length; i++)
                {
                    var openIndex = FindOpenIndex(text, i);
                    if (openIndex == -1) break;
                    i = openIndex;
                    brackets.Add(x++, new BracketModel { OpenBracket = openIndex });
                }
                foreach (var bracketModel in brackets)
                {
                    for (var i = bracketModel.Value.OpenBracket; i < text.Length; i++)
                    {
                        var closeIndex = FindCloseIndex(text, i);
                        if (closeIndex == -1) return brackets;
                        var substring = text.Substring(bracketModel.Value.OpenBracket,
                            closeIndex - (bracketModel.Value.OpenBracket - 1));
                        var allClose = substring.Count(e => e == '}' || e == ']');
                        var allOpen = substring.Count(e => e == '{' || e == '[');
                        if (allClose == allOpen)
                        {
                            bracketModel.Value.CloseBracket = closeIndex;
                            var outer = brackets.LastOrDefault(
                                q =>
                                    q.Value.CloseBracket > closeIndex &&
                                    q.Value.OpenBracket < bracketModel.Value.OpenBracket);
                            if (outer.Value != null)
                            {
                                bracketModel.Value.OuterBraket = outer.Key;
                                bracketModel.Value.BraketLevel = outer.Value.BraketLevel + 1;
                            }
                            break;
                        }
                        i = closeIndex;
                    }
                }
                return brackets;
            }
        }
        public JsonEditor()
        {
            InitializeComponent();
            shbox.TextChanged += Shbox_TextChanged;
            shbox.PreviewKeyDown += Shbox_PreviewKeyDown;
            shbox.SelectionChanged += Shbox_SelectionChanged;
            shbox.CurrentHighlighter = new Highlighter();
            _tabControl = new TabControl(tabControl, shbox);
            _tabControl.TabSelected += _tabControl_TabSelected;
            _tabControl.RemoveTab += _tabControl_RemoveTab;
            _tabControl.AddTab("New Tab");
            _timer.Stop();
            _timer.Elapsed += Timer_Elapsed;
        }
        public string DefaultQueryString
        {
            get { return (string)GetValue(DefaultQueryStringDependencyProperty); }

            set { SetValue(DefaultQueryStringDependencyProperty, value); }
        }
        public string DocumentString
        {
            get { return (string)GetValue(JsonStringDependencyProperty); }

            set { SetValue(JsonStringDependencyProperty, value); }
        }

        public string SelectedTabCode
        {
            get { return (string)GetValue(SelectedTabCodeDataDependencyProperty); }

            set { SetValue(SelectedTabCodeDataDependencyProperty, value); }
        }

        public bool Indent
        {
            get { return (bool)GetValue(IndentDependencyProperty); }

            set { SetValue(IndentDependencyProperty, value); }
        }

        public Query Query
        {
            get { return (Query)GetValue(QueryDependencyProperty); }

            set { SetValue(QueryDependencyProperty, Query); }
        }

        public string LoadTabsData
        {
            get { return (string)GetValue(LoadTabsDataDependencyProperty); }

            set { SetValue(LoadTabsDataDependencyProperty, value); }
        }

        public string SaveTabData
        {
            get { return (string)GetValue(SaveDataDependencyProperty); }

            set { SetValue(SaveDataDependencyProperty, value); }
        }

        private static void SelectedTabCodeChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
        }

        private void _tabControl_RemoveTab(Tab sender)
        {
            SelectedTabCode = sender.Code;
            OnDeleteTabEvent();
        }

        private static void QueryChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;


            eventsComboBox?.QueryChanged(dependencyPropertyChangedEventArgs);
        }

        private void QueryChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            UpdateIntelsence();
        }


        public event EventHandler SaveTabEvent;
        public event EventHandler LoadTabsEvent;
        public event EventHandler DeleteTabEvent;

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                _timer.Stop();
                ValidateString();
                UpdateIntelsence();
            });
        }

        private void ValidateString()
        {
            var isvalid = IsValidJson(shbox.Text);
            _tabControl.SelectedTab.TabItem.Foreground = !isvalid ? Brushes.Red : Brushes.Green;
        }

        private void Shbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && (Keyboard.IsKeyDown(Key.K) &&
                Keyboard.IsKeyDown(Key.C)))
            {
                IndentDocument();
            }
            else if (e.Key == Key.OemOpenBrackets)
            {
                var sq = !(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift));
                var start = shbox.SelectionStart;
                var text = ReplaceSpecialCharactersToSpace(shbox.Text);
                string lastWord;
                int startWord = WordAfter(text, out lastWord);
                shbox.Text = shbox.Text.Remove(startWord, lastWord.Length);
                shbox.Text = sq ? shbox.Text.Insert(startWord, $"[{lastWord}]") : shbox.Text.Insert(startWord, $"{{{lastWord}}}");
                shbox.SelectionStart = start + 1;

                e.Handled = true;


                //lstMethodsSelection.SelectedIndex = 0;
                //lstMethodsSelection.Focus();
            }
            else if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && Keyboard.IsKeyDown(Key.Space))
            {
                OpenPopup();
                //shbox
            }
            else if (e.Key == Key.Enter)
            {
                var brackets = GetBracketModels(shbox.Text);
                var start = shbox.SelectionStart;
                var withinbracket =
                    brackets.LastOrDefault(q => q.Value.OpenBracket <= start && q.Value.CloseBracket >= start);
                if (withinbracket.Value == null) return;
                var level = withinbracket.Value.BraketLevel;

                var space = GetLevelSpace(level);
                if (withinbracket.Value.CloseBracket == start)
                {
                    shbox.Text = shbox.Text.Insert(start, Environment.NewLine + GetLevelSpace(level - 1));
                    shbox.Text = shbox.Text.Insert(start, Environment.NewLine + space);



                }
                else
                {
                    shbox.Text = shbox.Text.Insert(start, Environment.NewLine + space);

                }

                shbox.SelectionStart = start + space.Length + 2;

                e.Handled = true;
            }
            else if ((e.Key == Key.Up || e.Key == Key.Down) && popup.IsOpen)
            {
                xListBox.SelectedIndex = 0;
                xListBox.Focus();
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                OpenPopup();
                shbox.Focus();
            }
            else if (e.Key == Key.Escape)
            {
                popup.IsOpen = false;
                shbox.Focus();
            }
        }

        private void OpenPopup()
        {
            var rec = shbox.GetRectFromCharacterIndex(shbox.CaretIndex, true);
            popup.PlacementRectangle = rec;
            popup.IsOpen = true;
            popup.StaysOpen = false;
            xListBox.SelectedIndex = 0;
            xListBox.Focus();
        }

        private int WordAfter(string text, out string lastWord)
        {
            lastWord = "";
            int wordEnd = text.IndexOf(' ', shbox.CaretIndex);
            if (wordEnd <= 0)
            {
                return 0;
            }
            int wordStart = text.LastIndexOf(' ', wordEnd - 1) + 1;

            lastWord = text.Substring(wordStart, wordEnd - wordStart);
            return wordStart;
        }
        private string GetLevelSpace(int level)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < level + 1; i++)
            {
                sb.Append("  ");
            }
            return sb.ToString();
        }

        private void IndentDocument()
        {
            var isvalid = IsValidJson(shbox.Text);
            if (isvalid)
            {
                dynamic onj = JsonConvert.DeserializeObject(shbox.Text);
                var selectionStart = shbox.SelectionStart;
                shbox.Text = JsonConvert.SerializeObject(onj, Formatting.Indented);
                shbox.SelectionStart = selectionStart;
            }
        }

        private void _tabControl_TabSelected(Tab sender)
        {
            _textLoading = true;
            DocumentString = _tabControl.SelectedTab.Text;
            _textLoading = false;
            SaveButton.Background = _tabControl.SelectedTab.IsLoaded ? Brushes.Green : Brushes.Red;
            ValidateString();
        }

        private void Shbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_textLoading)
                _tabControl.SelectedTab.IsLoaded = false;
            SaveButton.Background = _tabControl.SelectedTab.IsLoaded ? Brushes.Green : Brushes.Red;
            _tabControl.SelectedTab.Text = shbox.Text;
            DocumentString = _tabControl.SelectedTab.Text;
            _timer.Stop();
            _timer.Start();
        }

        private void Shbox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            _timer.Start();
        }

        private void UpdateIntelsence()
        {
            if (Query != null)
            {
                var query = FindQueryObject(shbox);
                var currentWord = GetCurrentWord(shbox);
                var q = Query.GetQueryFor(query, currentWord);
                xListBox.ItemsSource = q;
                if (q.Count == 0)
                {
                    popup.IsOpen = false;
                }
            }
        }

        private string FindQueryObject(SyntaxHighlightBox syntaxHighlightBox)
        {
            var query = DefaultQueryString;

            var start = syntaxHighlightBox.SelectionStart;
            var text = ReplaceSpecialCharactersToSpace(syntaxHighlightBox.Text, true);
            var brackets = GetBracketModels(text);
            var withinbracket =
                brackets.LastOrDefault(q => q.Value.OpenBracket <= start && q.Value.CloseBracket >= start);
   

            if (withinbracket.Value != null)
            {

                var outerBraketindex = withinbracket.Value.OuterBraket;
                if (outerBraketindex <= -1) return query;
                Console.WriteLine(text.Substring(withinbracket.Value.OpenBracket, withinbracket.Value.CloseBracket - (withinbracket.Value.OpenBracket - 1)));
                var endW = text.LastIndexOf(":", withinbracket.Value.OpenBracket, StringComparison.Ordinal);
                if (endW == -1) return query;
                var sstring = text.Substring(endW, withinbracket.Value.OpenBracket - (endW));
                if (sstring.IndexOf("}", StringComparison.Ordinal) != -1)
                {

                    var outerBraket = brackets[withinbracket.Value.OuterBraket];
                    endW = text.LastIndexOf(":", outerBraket.OpenBracket, StringComparison.Ordinal);
                }
                if (endW != -1)
                {
                    var startW = text.LastIndexOf(" ", endW, StringComparison.Ordinal);
                    //Console.WriteLine(text.Substring(startW, endW - startW));

                    query = RemoveSpecialCharacters(text.Substring(startW, endW - startW));
                }
            }
            return query;
        }

        private Dictionary<int, BracketModel> GetBracketModels(string text)
        {
            var x = 0;
            var brackets = new Dictionary<int, BracketModel>();
            try
            {
                for (var i = 0; i < text.Length; i++)
                {
                    var openIndex = FindOpenIndex(text, i);
                    if (openIndex == -1) break;
                    i = openIndex;
                    brackets.Add(x++, new BracketModel { OpenBracket = openIndex });
                }
                foreach (var bracketModel in brackets)
                {
                    for (var i = bracketModel.Value.OpenBracket; i < text.Length; i++)
                    {
                        var closeIndex = FindCloseIndex(text, i);
                        var substring = text.Substring(bracketModel.Value.OpenBracket,
                            closeIndex - (bracketModel.Value.OpenBracket - 1));
                        var allClose = substring.Count(e => e == '}' || e == ']');
                        var allOpen = substring.Count(e => e == '{' || e == '[');
                        if (allClose == allOpen)
                        {
                            bracketModel.Value.CloseBracket = closeIndex;
                            var outer = brackets.LastOrDefault(
                                q =>
                                    q.Value.CloseBracket > closeIndex &&
                                    q.Value.OpenBracket < bracketModel.Value.OpenBracket);
                            if (outer.Value != null)
                            {
                                bracketModel.Value.OuterBraket = outer.Key;
                                bracketModel.Value.BraketLevel = outer.Value.BraketLevel + 1;
                            }
                            break;
                        }
                        i = closeIndex;
                    }
                }
            }
            catch (Exception)
            {
                
            }


            return brackets;
        }


        private int FindOpenIndex(string text, int i)
        {
            var firstopenIndex = text.IndexOf("{", i, StringComparison.Ordinal);
            var secondopenIndex = text.IndexOf("[", i, StringComparison.Ordinal);
            return firstopenIndex < secondopenIndex || secondopenIndex == -1 ? firstopenIndex : secondopenIndex;
        }

        private int FindCloseIndex(string text, int i)
        {
            var firstCloseIndex = text.IndexOf("}", i, StringComparison.Ordinal);
            var secondCloseIndex = text.IndexOf("]", i, StringComparison.Ordinal);
            return firstCloseIndex < secondCloseIndex || secondCloseIndex == -1 ? firstCloseIndex : secondCloseIndex;
        }

        private void XListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void XListBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PopupClicked();
        }

        private void PopupClicked()
        {
            var query = (QueryItem)xListBox.SelectedItem;
            var start = shbox.SelectionStart;
            var text = shbox.Text;
            var brackets = GetBracketModels(text);
            var withinbracket =
                brackets.LastOrDefault(q => q.Value.OpenBracket <= start && q.Value.CloseBracket >= start);
            var cwordindex = start;
            var currentWord = GetCurrentWord(shbox);
            if (currentWord != "")
            {
                cwordindex = text.LastIndexOf(currentWord, start + 1, StringComparison.Ordinal);
                text = text.Remove(cwordindex, currentWord.Length);
            }
            var textToInsert = $"\"{query.Query}\"" + " : " + query.Default;
            shbox.Text = text.Insert(cwordindex, textToInsert);
            IndentDocument();

            shbox.Focus();
            brackets = GetBracketModels(shbox.Text);
            var setCourseBacketIndex = brackets.ToList().FindAll(q => q.Value.OpenBracket > cwordindex);
            if (setCourseBacketIndex.Count != 0)
            {
                var i = setCourseBacketIndex.ToList().Max(q => q.Value.OuterBraket);
                var setCourseBacket = brackets.FirstOrDefault(q => q.Value.OuterBraket == i);
                popup.IsOpen = false;
                shbox.SelectionStart = setCourseBacket.Value.OpenBracket + 1;
            }
            else
            {
                popup.IsOpen = false;
                shbox.SelectionStart = start;
            }
        }


        private string GetCurrentWord(SyntaxHighlightBox syntaxHighlightBox)
        {
            var start = syntaxHighlightBox.SelectionStart;

            var text = ReplaceSpecialCharactersToSpace(syntaxHighlightBox.Text, true);

            if (start == 0 || text.Substring(start - 1, 1) == " ") return string.Empty;

            var whiteSpaceIndex = text.IndexOf(" ", start, StringComparison.Ordinal);
            if (whiteSpaceIndex < 0)
            {
                return string.Empty;
            }
            var deleteText = text.Substring(0, whiteSpaceIndex).TrimEnd();
            whiteSpaceIndex = deleteText.LastIndexOf(" ", deleteText.Length, StringComparison.Ordinal);
            if (whiteSpaceIndex < 0)
            {
                return string.Empty;
            }
            deleteText = deleteText.Substring(whiteSpaceIndex, deleteText.Length - whiteSpaceIndex);
            return RemoveSpecialCharacters(deleteText).Trim();
        }

        public static string RemoveSpecialCharacters(string str, bool keepBracket = false)
        {
            var sb = new StringBuilder();
            foreach (var c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' ||
                    (keepBracket && (c == '{' || c == '}' || c == '[' || c == ']')))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string ReplaceSpecialCharactersToSpace(string str, bool keepBracket = false)
        {
            var sb = new StringBuilder();
            foreach (var c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' ||
                    c == '"' || c == ':' || (keepBracket && (c == '{' || c == '}' || c == '[' || c == ']')))
                {
                    sb.Append(c);
                }
                else
                {
                    sb.Append(' ');
                }
            }
            return sb.ToString();
        }

        private static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if (strInput.StartsWith("{") && strInput.EndsWith("}"))
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            return false;
        }

        private static void DefaultQueryStringChanged(DependencyObject dependencyObject,
     DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;

            eventsComboBox?.DefaultQueryStringChanged(dependencyPropertyChangedEventArgs);
        }

        private void DefaultQueryStringChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {

        }

        private static void SaveDataChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;

            eventsComboBox?.SaveDataChanged(dependencyPropertyChangedEventArgs);
        }

        private void SaveDataChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
        }

        private static void LoadTabsDataChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;

            eventsComboBox?.LoadTabsDataChanged(dependencyPropertyChangedEventArgs);
        }

        private void LoadTabsDataChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (!string.IsNullOrEmpty(LoadTabsData))
            {
                var tabs = JsonConvert.DeserializeObject<List<Tab>>(LoadTabsData);
                foreach (var tab in tabs)
                {
                    _tabControl.AddTab(tab.Header, tab.Text, tab.Code, tab.IsLoaded);
                }
            }
        }

        private static void DocumentStringChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;
            eventsComboBox?.DocumentStringChanged(dependencyPropertyChangedEventArgs);
        }

        public void DocumentStringChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            shbox.Text = DocumentString;
        }

        private static void IndentChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonEditor;

            eventsComboBox?.IndentChanged(dependencyPropertyChangedEventArgs);
        }

        public void IndentChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (Indent)
            {
                IndentDocument();
            }
        }

        private void AddTab()
        {
            _tabControl.AddTab("New Tab", "{\n}");
        }


        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            AddTab();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var name = Interaction.InputBox("Please input the Tab Name.", "Elastic Shell",
                _tabControl.SelectedTab.Header, -1, -1);
            if (!string.IsNullOrEmpty(name))
            {
                _tabControl.SelectedTab.Header = name;
                _tabControl.SelectedTab.IsLoaded = true;
                SaveButton.Background = _tabControl.SelectedTab.IsLoaded ? Brushes.Green : Brushes.Red;
                SaveTabData = JsonConvert.SerializeObject(_tabControl.SelectedTab);
                OnSaveTabEvent();
            }
        }

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            LoadTabsData = "";
            OnLoadTabsEvent();
        }

        protected virtual void OnSaveTabEvent()
        {
            SaveTabEvent?.Invoke(this, null);
        }

        protected virtual void OnLoadTabsEvent()
        {
            LoadTabsEvent?.Invoke(this, null);
        }

        protected virtual void OnDeleteTabEvent()
        {
            DeleteTabEvent?.Invoke(this, EventArgs.Empty);
        }

        public class BracketModel
        {
            public BracketModel()
            {
                OuterBraket = -1;
            }
            public int Index { get; set; }
            public int OuterBraket { get; set; }
            public int BraketLevel { get; set; }

            public int OpenBracket { get; set; }
            public int CloseBracket { get; set; }
        }

        private void XListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PopupClicked();
                e.Handled = true;
            }
            else if (!(e.Key == Key.Up || e.Key == Key.Down))
            {
                shbox.Focus();
            }
            else if (e.Key == Key.Escape)
            {
                popup.IsOpen = false;
                shbox.Focus();
            }

        }

        private void Popup_OnGotFocus(object sender, RoutedEventArgs e)
        {
           // xListBox.Focus();
        }

        private void XListBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (xListBox.SelectedIndex != -1)
                ((ListBoxItem)xListBox.ItemContainerGenerator.ContainerFromIndex(xListBox.SelectedIndex)).Focus();
            else
                xListBox.Focus();
        }
    }
}