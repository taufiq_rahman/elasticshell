using System.Collections.Generic;
using System.Windows.Controls;
using AurelienRibon.Ui.SyntaxHighlightBox;

namespace ElasticShell.Components
{
    internal class TabControl
    {
        public delegate void TabSelectedHandler(Tab sender);

        private readonly System.Windows.Controls.TabControl _tabControl;
        private readonly SyntaxHighlightBox _textBox;

        public TabControl(System.Windows.Controls.TabControl tabControl, SyntaxHighlightBox textBox)
        {
            Tabs = new List<Tab>();
            _tabControl = tabControl;
            _textBox = textBox;
            _tabControl.SelectionChanged += _tabControl_SelectionChanged;
        }

        public Tab SelectedTab { get; set; }

        public List<Tab> Tabs { get; set; }

        public event TabSelectedHandler TabSelected;
        public event TabSelectedHandler RemoveTab;

        private void _tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_tabControl.SelectedItem == null) return;
            SelectedTab = Tabs.Find(q => Equals(q.TabItem, _tabControl.SelectedItem));
            OnTabSelected(SelectedTab);
        }

        public void AddTab(string header)
        {
            var tab = new Tab(header);
            tab.RemoveTab += Tab_RemoveTab;
            tab.TabSelected += Tab_TabSelected;
            Tabs.Add(tab);
            _tabControl.Items.Add(tab.TabItem);
            _tabControl.SelectedItem = tab.TabItem;
        }

        public void AddTab(string header, string text)
        {
            var tab = new Tab(header);
            tab.Text = text;
            tab.RemoveTab += Tab_RemoveTab;
            tab.TabSelected += Tab_TabSelected;
            Tabs.Add(tab);
            _tabControl.Items.Add(tab.TabItem);
            _tabControl.SelectedItem = tab.TabItem;
        }

        public void AddTab(string header, string text, string code, bool isloaded)
        {
            var tab = Tabs.Find(q => q.Code == code);
            if (tab == null)
            {
                tab = new Tab(header);
                tab.Text = text;
                tab.Code = code;
                tab.IsLoaded = true;
                tab.RemoveTab += Tab_RemoveTab;
                tab.TabSelected += Tab_TabSelected;
                Tabs.Add(tab);
                _tabControl.Items.Add(tab.TabItem);
            }
            else
            {
                tab.Text = text;
                tab.Header = header;
                tab.Code = code;
                tab.IsLoaded = true;
            }

            //_tabControl.SelectedItem = tab.TabItem;
        }
        private void Tab_TabSelected(Tab sender)
        {
        }

        private void Tab_RemoveTab(Tab sender)
        {
            if (sender.IsLoaded)
            {
                OnRemoveTab(sender);
            }
            if (Tabs.Count == 1)
            {
                sender.TabItem.Header = "New Tab";
                sender.Text = "";
                _textBox.Text = "";
            }
            else
            {
                Tabs.Remove(sender);
                _tabControl.Items.Remove(sender.TabItem);
            }
        }

        protected virtual void OnTabSelected(Tab sender)
        {
            TabSelected?.Invoke(sender);
        }

        protected virtual void OnRemoveTab(Tab sender)
        {
            RemoveTab?.Invoke(sender);
        }
    }
}