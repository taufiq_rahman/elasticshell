﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AurelienRibon.Ui.SyntaxHighlightBox;

namespace ElasticShell.Components
{
    /// <summary>
    ///     Interaction logic for SmartJsonTextBox.xaml
    /// </summary>
    public partial class SmartJsonTextBox : UserControl
    {
        public static readonly DependencyProperty SelectedItemDependencyProperty =
            DependencyProperty.Register(
                "SelectedItem",
                typeof(string),
                typeof(SmartJsonTextBox),
                new PropertyMetadata(SelectedItemChanged));

        public static readonly DependencyProperty TextDependencyProperty =
            DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(SmartJsonTextBox),
                new PropertyMetadata(TextChanged));

        public static readonly DependencyProperty SearchTextDependencyProperty =
            DependencyProperty.Register(
                "SearchText",
                typeof(string),
                typeof(SmartJsonTextBox),
                new PropertyMetadata(SearchTextChanged));

        public SmartJsonTextBox()
        {
            InitializeComponent();
            _highlighter = new Highlighter();
            SyntaxHighlightBox.SelectionChanged += SyntaxHighlightBox_SelectionChanged;
            SyntaxHighlightBox.CurrentHighlighter = _highlighter;
            timer.Stop();
            timer.Interval = 500;
            timer.Elapsed += Timer_Elapsed;
  
        }


        private Highlighter _highlighter;

        class Highlighter : IHighlighter
        {
            public string SearchText { get; set; }
            public int Highlight(FormattedText text, int previousBlockCode)
            {

                //for (int i = 0; i < text.Text.Length; i++)
                //{

                //    var eindex = text.Text.IndexOf(":", i, StringComparison.Ordinal);

                //    if (eindex != -1)
                //    {
                //        var index = text.Text.LastIndexOf(" ", eindex, StringComparison.Ordinal);
                //        var valueindex = text.Text.IndexOf(",", eindex, StringComparison.Ordinal);
                //        text.SetForegroundBrush(Brushes.Red, index, eindex - index);
                //        text.SetForegroundBrush(Brushes.ForestGreen, eindex+1, valueindex - eindex+1);

                //        i = eindex;
                //    }
                //    else
                //    {
                //        break;

                //    }
                //}
                if (SearchText != null)
                {
                    for (int i = 0; i < text.Text.Length; i++)
                    {
                        var index = text.Text.IndexOf(SearchText, i, StringComparison.Ordinal);
                        if (index != -1)
                        {
                            text.SetFontWeight(FontWeights.Bold, index, SearchText.Length);
                            text.SetForegroundBrush(Brushes.DeepSkyBlue, index, SearchText.Length);
                            i = index;
                        }
                        else
                        {
                            break;

                        }
                    }
                }
                return previousBlockCode;
            }
        }
        public string SearchText
        {
            get { return (string)GetValue(SearchTextDependencyProperty); }

            set { SetValue(SearchTextDependencyProperty, value); }
        }


        public string SelectedItem
        {
            get { return (string)GetValue(SelectedItemDependencyProperty); }

            set { SetValue(SelectedItemDependencyProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextDependencyProperty); }

            set { SetValue(TextDependencyProperty, value); }
        }

        private void SyntaxHighlightBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            SelectedItem = SyntaxHighlightBox.SelectedText;
        }


        private static void SearchTextChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as SmartJsonTextBox;
            eventsComboBox?.SearchTextChanged(dependencyPropertyChangedEventArgs);
        }
        Timer timer = new Timer();
        private void SearchTextChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (SearchText != "")
            {
                timer.Stop();
                timer.Start();
            }

        }
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                timer.Stop();
                _highlighter.SearchText = SearchText;
                var d = SyntaxHighlightBox.VerticalOffset;
                SyntaxHighlightBox.Text = Text + "1";
                SyntaxHighlightBox.Text = Text;
                SyntaxHighlightBox.ScrollToVerticalOffset(d);
            });

        }

        private static void TextChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as SmartJsonTextBox;


            eventsComboBox?.TextChanged(dependencyPropertyChangedEventArgs);
        }

        private void TextChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            SyntaxHighlightBox.Text = Text;
        }


        private static void SelectedItemChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as SmartJsonTextBox;
            eventsComboBox?.SelectedItemChanged(dependencyPropertyChangedEventArgs);
        }

        private void SelectedItemChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
        }
    }
}