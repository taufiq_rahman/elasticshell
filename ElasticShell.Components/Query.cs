﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ElasticShell.Components
{

    public class Query
    {
        public List<QueryItem> QueryItems { get; set; }

        public List<QueryItem> GetQueryFor(string inside, string query)
        {
            if(string.IsNullOrEmpty(query))
                return QueryItems.FindAll(q => q.Inside.ToList().Exists(e => e.Equals(inside))).ToList();
            else
                return QueryItems.FindAll(q => q.Inside.ToList().Exists(e => e.Equals(inside)) && q.Query.Contains(query)).ToList();
        }

    }

    public class QueryItem
    {
        [JsonProperty(PropertyName = "inside")]
        public string[] Inside { get; set; }

        [JsonProperty(PropertyName = "query")]
        public string Query { get; set; }

        [JsonProperty(PropertyName = "default")]
        public object Default { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "values")]
        public string[] Values { get; set; }
    }

}
