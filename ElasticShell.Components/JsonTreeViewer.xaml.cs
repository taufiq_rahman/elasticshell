﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ElasticShell.Components
{
    /// <summary>
    ///     Interaction logic for JsonTreeViewer.xaml
    /// </summary>
    public partial class JsonTreeViewer : UserControl
    {
        public static readonly DependencyProperty JsonStringDependencyProperty =
            DependencyProperty.Register(
                "JsonString",
                typeof(string),
                typeof(JsonTreeViewer),
                new PropertyMetadata(JsonStringLoaded));

        public static readonly DependencyProperty ShowArrayIndexDependencyProperty =
            DependencyProperty.Register(
                "ShowArrayIndex",
                typeof(bool),
                typeof(JsonTreeViewer),
                new PropertyMetadata(ShowArrayIndexChanged));

        public static readonly DependencyProperty SelectedItemDependencyProperty =
            DependencyProperty.Register(
                "SelectedItem",
                typeof(string),
                typeof(JsonTreeViewer),
                new PropertyMetadata(SelectedItemChanged));

        public static readonly DependencyProperty SearchTextDependencyProperty =
            DependencyProperty.Register(
                "SearchText",
                typeof(string),
                typeof(JsonTreeViewer),
                new PropertyMetadata(SearchTextChanged));


        private bool _showArrayIndex;

        public JsonTreeViewer()
        {
            InitializeComponent();
            JTreeView.SelectedItemChanged += JTreeView_SelectedItemChanged;
        }

        public string SearchText
        {
            get { return (string)GetValue(SearchTextDependencyProperty); }

            set { SetValue(SearchTextDependencyProperty, value); }
        }

        public string SelectedItem
        {
            get { return (string)GetValue(SelectedItemDependencyProperty); }

            set { SetValue(SelectedItemDependencyProperty, value); }
        }

        public string JsonString
        {
            get { return (string)GetValue(JsonStringDependencyProperty); }

            set { SetValue(JsonStringDependencyProperty, value); }
        }

        public bool ShowArrayIndex
        {
            get { return (bool)GetValue(ShowArrayIndexDependencyProperty); }

            set { SetValue(ShowArrayIndexDependencyProperty, value); }
        }

        private void JTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var tab = JTreeView.SelectedItem as TreeViewItem;

            if (tab != null)
            {
                dynamic obj = new ExpandoObject();
                var objType = (FakeTreeViewItem.ObjectType)tab.Tag;
                if (objType == FakeTreeViewItem.ObjectType.Array)
                    obj = new ArrayList();
                obj = TreeToObject(tab, null);
                SelectedItem = JsonConvert.SerializeObject(obj, Formatting.Indented);
            }
            else
            {
                SelectedItem = "";
            }
        }

        private dynamic TreeToObject(TreeViewItem roottab, dynamic rootObj)
        {
            var objType = (FakeTreeViewItem.ObjectType)roottab.Tag;
            if (objType == FakeTreeViewItem.ObjectType.Array)
            {
                var arr = new ArrayList();
                foreach (TreeViewItem item in roottab.Items)
                {
                    dynamic obj = new ExpandoObject();
                    arr.Add(TreeToObject(item, obj));
                }
                var prop = roottab.Header.ToString().Split(':');
                if (rootObj == null)
                {
                    rootObj = arr;
                }
                else
                {
                    ((IDictionary<string, dynamic>)rootObj).Add(prop[0], arr);
                }
            }
            else if (objType == FakeTreeViewItem.ObjectType.Object)
            {
                if (rootObj == null)
                {
                    rootObj = new ExpandoObject();
                }

                foreach (TreeViewItem item in roottab.Items)
                {
                    var type = (FakeTreeViewItem.ObjectType)item.Tag;


                    if (type == FakeTreeViewItem.ObjectType.Object)
                    {
                        var obj = new ExpandoObject();
                        var prop = item.Header.ToString().Split(':');
                        ((IDictionary<string, object>)rootObj).Add(prop[0], TreeToObject(item, obj));
                    }
                    else
                    {
                        TreeToObject(item, rootObj);
                    }
                }
            }
            else
            {
                if (objType == FakeTreeViewItem.ObjectType.Property)
                {
                    if (rootObj == null)
                    {
                        rootObj = new ExpandoObject();
                    }
                    var prop = roottab.Header.ToString().Split(':');
                    var val = string.Empty;
                    if (prop.Length > 2)
                    {
                        for (var i = 1; i < prop.Length; i++)
                        {
                            val = val + ":" + prop[i];
                        }
                    }
                    else
                    {
                        val = prop[1];
                    }
                    ((IDictionary<string, object>)rootObj).Add(prop[0], val.Substring(1));
                }
                else
                {
                    var val = roottab.Header.ToString().Contains("|")
                        ? roottab.Header.ToString().Remove(0, roottab.Header.ToString().IndexOf("|") + 2)
                        : roottab.Header.ToString();
                    rootObj = val;
                }
            }
            return rootObj;
        }

        private static void SearchTextChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonTreeViewer;
            eventsComboBox?.SearchTextChanged(dependencyPropertyChangedEventArgs);
        }

        private static void JsonStringLoaded(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonTreeViewer;
            eventsComboBox?.JsonStringLoaded(dependencyPropertyChangedEventArgs);
        }

        private static void ShowArrayIndexChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonTreeViewer;

            eventsComboBox?.ShowArrayIndexChanged(dependencyPropertyChangedEventArgs);
        }

        private static void SelectedItemChanged(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var eventsComboBox = dependencyObject as JsonTreeViewer;


            eventsComboBox?.SelectedItemChanged(dependencyPropertyChangedEventArgs);
        }

        private void SearchTextChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            SearchTree();
        }

        private void SearchTree()
        {
            if (JTreeView.Items.Count > 0)
            {
                ItemCollection items = JTreeView.Items;
                foreach (TreeViewItem node in items)
                {
                    Search(node);
                }
            }
        }

        private bool Search(TreeViewItem item)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(SearchText) && item.Header.ToString().Contains(SearchText))
            {
                item.Background = Brushes.LightSkyBlue;
                result = true;
            }
            else
            {
                item.Background = Brushes.Transparent;
            }
            foreach (TreeViewItem node in item.Items)
            {
                var pre = Search(node);
                result = pre || result;
                item.Background = result ? Brushes.LightSkyBlue : item.Background = Brushes.Transparent; ;
            }

            return result;
        }

        private void SelectedItemChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
        }

        private void ShowArrayIndexChanged(DependencyPropertyChangedEventArgs e)
        {
            _showArrayIndex = ShowArrayIndex;
            if (JsonString != null)
            {
                LoadDocument();
            }
        }

        private void JsonStringLoaded(DependencyPropertyChangedEventArgs e)
        {
            LoadDocument();

        }

        private void LoadDocument()
        {
            //MessageBox.Show("LoadDocument");

            LoadingGrid.Visibility = Visibility.Visible;
            JTreeView.Items.Clear();
            if (string.IsNullOrEmpty(JsonString)) return;
 
            dynamic jobject = JsonConvert.DeserializeObject(JsonString);
    
            var t = Task.Factory.StartNew(() =>
            {
                var items = new FakeTreeViewItem();

                if (jobject is JArray)
                {
                    var array = jobject as JArray;
                    items.Header = $"[{array.Count}]";
                    items.Type = FakeTreeViewItem.ObjectType.Array;
                }
                else if (jobject is JObject)
                {
                    var array = jobject as JObject;
                    items.Header = "{..}";
                    items.Type = FakeTreeViewItem.ObjectType.Object;
                }
                var index = 0;

                foreach (var kvp in jobject)
                {
                    FakeTreeViewItem item = null;
                    if (kvp is JObject)
                    {
                        index++;
                        item = new FakeTreeViewItem
                        {
                            Header = _showArrayIndex ? index + "| {..}" : "{..}",
                            Type = FakeTreeViewItem.ObjectType.Object
                        };
                        LoopThrough(item, kvp);
                    }
                    else if (kvp is JProperty)
                    {
                        var prop = kvp as JProperty;
                        item = new FakeTreeViewItem();

                        if (prop.Value is JObject)
                        {
                            item.Header = $"{prop.Name}: {{..}}";
                            LoopThrough(item, prop.Value.Value<JObject>());
                        }
                        else
                        {
                            item.Header = $"{prop.Name}: {prop.Value.Value<string>()}";
                            item.Type = FakeTreeViewItem.ObjectType.Property;
                        }


                    }
                    items.Items.Add(item);
                }
                return items;
            });
            t.GetAwaiter().OnCompleted(() => { YourMethod(t.Result); });
        }

        private void YourMethod(FakeTreeViewItem rootitem)
        {
            if (Application.Current.Dispatcher.CheckAccess())
            {
                var tItem = new TreeViewItem
                {
                    IsExpanded = true,
                    Header = rootitem.Header,
                    Tag = rootitem.Type
                };
                FillItems(rootitem, tItem);

                JTreeView.Items.Add(tItem);
                LoadingGrid.Visibility = Visibility.Collapsed;
                SearchTree();
            }
            else
            {
                //Other wise re-invoke the method with UI thread access
                Application.Current.Dispatcher.Invoke(() => YourMethod(rootitem));
            }
        }

        private void FillItems(FakeTreeViewItem fitem, TreeViewItem tItem)
        {
            foreach (var fakeTreeViewItem in fitem.Items)
            {
                var titem = new TreeViewItem
                {
                    Header = fakeTreeViewItem.Header,
                    Tag = fakeTreeViewItem.Type
                };
                FillItems(fakeTreeViewItem, titem);
                tItem.Items.Add(titem);
            }
        }

        private void LoopThrough(FakeTreeViewItem treeViewItem, JObject rootJObject)
        {
            foreach (var x in rootJObject.Properties())
            {
                var item = new FakeTreeViewItem();


                if (x.Value is JObject)
                {
                    item.Header = x.Name + " : {..}";
                    item.Type = FakeTreeViewItem.ObjectType.Object;
                    LoopThrough(item, x.Value.Value<JObject>());
                }
                else if (x.Value is JArray)
                {
                    item.Header = $"{x.Name} : [{((JArray)x.Value).Count}]";
                    item.Type = FakeTreeViewItem.ObjectType.Array;

                    var index = 0;
                    foreach (var jToken in x.Value.Value<JArray>())
                    {
                        index++;
                        if (jToken is JObject)
                        {
                            var node = (JObject)jToken;
                            var nitem = new FakeTreeViewItem { Header = _showArrayIndex ? index + "| {..}" : "{..}" };
                            nitem.Type = FakeTreeViewItem.ObjectType.Object;
                            LoopThrough(nitem, node);
                            item.Items.Add(nitem);
                        }
                        else
                        {
                            var nitem = new FakeTreeViewItem();
                            var value = jToken.ToString();
                            nitem.Type = FakeTreeViewItem.ObjectType.Value;
                            nitem.Header = _showArrayIndex ? index + "| " + value : value;
                            item.Items.Add(nitem);
                        }
                    }
                }
                else
                {
                    var value = x.Value.Value<string>();
                    item.Type = FakeTreeViewItem.ObjectType.Property;
                    item.Header = x.Name + ": " + value;
                }
                treeViewItem.Items.Add(item);
            }
            // var v = rootJObject[0];
        }

        private class FakeTreeViewItem
        {
            public enum ObjectType
            {
                Array = 0,
                Object = 1,
                Property = 2,
                Value = 3
            }

            public FakeTreeViewItem()
            {
                Items = new List<FakeTreeViewItem>();
            }

            public ObjectType Type { get; set; }
            public string Header { get; set; }
            public List<FakeTreeViewItem> Items { get; }
        }
    }
}